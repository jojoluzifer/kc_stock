<?php

function option_EMDept() {
    $sql = "SELECT DeptID, DeptName,DeptCode FROM EMDept";
    $result = db_query($sql)->fetchAll();
    $array = array();
    foreach ($result as $value) {
        $array[$value->DeptID] = $value->DeptName . " " . $value->DeptCode;
    }
    return $array;
}

function manage_min_stock_add_form($form, $form_state, $brchid, $inveid, $goodid) {
    $EMInve = reset(entity_load('EMInve', array($inveid), array()));
    $EMBrch = reset(entity_load('EMBrch', array($brchid), array()));
    $EMGood = reset(entity_load('EMGood', array($goodid), array()));
    drupal_set_title("ตั้งค่า " . $EMGood->GoodName1);

//    $EMGoodType = reset(entity_load('EMGoodType', array($goodtype), array()));
//    $EMGoodGroup = reset(entity_load('EMGoodGroup', array($goodgroup), array()));
//    $EMDept = reset(entity_load('EMDept', array($deptid), array()));
//    $form['DeptID'] = array(
//        '#type' => 'hidden',
//        '#default_value' => $deptid,
//    );

    $sql_order = "SELECT ListNo,GoodID,InveID,BrchID,MinPoint,MaxPoint FROM EMGoodOrder where InveID = $inveid and BrchID = $brchid and GoodID = $goodid ";
    $result_order = db_query($sql_order)->fetchAll();
    foreach ($result_order as $value_order) {
        $value_order->ListNo;
        $value_order->MinPoint;
        $value_order->MaxPoint;
    }


    if ($result_order != NULL) {
        $form['edit_minpoint_fieldset'] = array(
            '#type' => 'fieldset',
            '#title' => t('แก้ไข ตั้งค่าจำนวนต่ำสุดของวัตถุดิบ'),
        );
        $form['edit_minpoint_fieldset']['MinPoint'] = array(
            '#type' => 'textfield',
            '#title' => t('ตั้งค่า MinPoint'),
            '#maxlength' => 64,
            '#size' => 15,
            '#default_value' => $value_order->MinPoint,
        );
        $form['edit_minpoint_fieldset']['MaxPoint'] = array(
            '#type' => 'textfield',
            '#title' => t('ตั้งค่า MaxPoint'),
            '#maxlength' => 64,
            '#size' => 15,
            '#default_value' => $value_order->MaxPoint,
        );
        $form['detail_fieldset'] = array(
            '#type' => 'fieldset',
            '#title' => t('รายละเอียดการ ตั้งค่าจำนวนต่ำสุดของวัตถุดิบ'),
        );



        $form['ListNo'] = array(
            '#type' => 'hidden',
            '#default_value' => $value_order->ListNo,
        );


        $form['check_order'] = array(
            '#type' => 'hidden',
            '#default_value' => 0,
        );

        $form['InveID'] = array(
            '#type' => 'hidden',
            '#default_value' => $inveid,
        );

        $form['BrchID'] = array(
            '#type' => 'hidden',
            '#default_value' => $brchid,
        );

        $form['GoodID'] = array(
            '#type' => 'hidden',
            '#default_value' => $goodid,
        );
        $form['GoodGroupID'] = array(
            '#type' => 'hidden',
            '#default_value' => $EMGood->GoodGroupID,
        );

        $form['detail_fieldset']['InveName'] = array(
            '#type' => 'textfield',
            '#title' => t('คลัง'),
            '#default_value' => $EMInve->InveName,
            '#size' => 50,
            '#disabled' => TRUE,
        );
        $form['detail_fieldset']['BrchName'] = array(
            '#type' => 'textfield',
            '#title' => t('สาขา'),
            '#default_value' => $EMBrch->BrchName,
            '#size' => 50,
            '#disabled' => TRUE,
        );
//        $form['detail_fieldset']['DeptID'] = array(
//            '#type' => 'textfield',
//            '#title' => t('แผนก'),
//            '#default_value' => $EMDept->DeptName,
//            '#size' => 50,
//            '#disabled' => TRUE,
//        );
//        $form['detail_fieldset']['GoodTypeName'] = array(
//            '#type' => 'textfield',
//            '#title' => t('ประเภทรายการสินค้า'),
//            '#default_value' => $EMGoodType->GoodTypeName,
//            '#size' => 50,
//            '#disabled' => TRUE,
//        );
//        $form['detail_fieldset']['GoodGroupName'] = array(
//            '#type' => 'textfield',
//            '#title' => t('กลุ่มสินค้า'),
//            '#default_value' => $EMGoodGroup->GoodGroupName,
//            '#size' => 50,
//            '#disabled' => TRUE,
//        );


        $form['detail_fieldset']['GoodName1'] = array(
            '#type' => 'textfield',
            '#title' => t('รายการสินค้า'),
            '#default_value' => $EMGood->GoodName1,
            '#size' => 50,
            '#disabled' => TRUE,
        );



        $form['submit'] = array('#type' => 'submit',
            '#value' => t('บันทึก'));
//        $form['cancel'] = array(
//            '#type' => 'button',
//            '#value' => t('ยกเลิก'),
//            '#weight' => 20,
//            '#page callback' => "show-branch",
//                //    '#submit' => array('mymodule_form_cancel1'),
//        );
        $form['clear'] = array(
            '#name' => 'clear',
            '#type' => 'button',
            '#value' => t('Reset'),
            '#attributes' => array('onclick' => 'this.form.reset(); return false;'),
        );
        return $form;
    } else {
        $form['add_minpoint_fieldset'] = array(
            '#type' => 'fieldset',
            '#title' => t('ตั้งค่าจำนวนต่ำสุดของวัตถุดิบ'),
        );
        $form['add_minpoint_fieldset']['MinPoint'] = array(
            '#type' => 'textfield',
            '#title' => t('ตั้งค่า MinPoint'),
            '#maxlength' => 64,
            '#size' => 15,
        );
        $form['add_minpoint_fieldset']['MaxPoint'] = array(
            '#type' => 'textfield',
            '#title' => t('ตั้งค่า MaxPoint'),
            '#maxlength' => 64,
            '#size' => 15,
        );

        $form['detail_fieldset'] = array(
            '#type' => 'fieldset',
            '#title' => t('รายละเอียดการ ตั้งค่าจำนวนต่ำสุดของวัตถุดิบ'),
        );


        $form['check_order'] = array(
            '#type' => 'hidden',
            '#default_value' => 1,
        );

        $form['InveID'] = array(
            '#type' => 'hidden',
            '#default_value' => $inveid,
        );

        $form['BrchID'] = array(
            '#type' => 'hidden',
            '#default_value' => $brchid,
        );

        $form['GoodID'] = array(
            '#type' => 'hidden',
            '#default_value' => $goodid,
        );
        $form['GoodGroupID'] = array(
            '#type' => 'hidden',
            '#default_value' => $EMGood->GoodGroupID,
        );

        $form['detail_fieldset']['InveName'] = array(
            '#type' => 'textfield',
            '#title' => t('คลัง'),
            '#default_value' => $EMInve->InveName,
            '#size' => 50,
            '#disabled' => TRUE,
        );
        $form['detail_fieldset']['BrchName'] = array(
            '#type' => 'textfield',
            '#title' => t('สาขา'),
            '#default_value' => $EMBrch->BrchName,
            '#size' => 50,
            '#disabled' => TRUE,
        );
//        $form['detail_fieldset']['DeptID'] = array(
//            '#type' => 'textfield',
//            '#title' => t('แผนก'),
//            '#default_value' => $EMDept->DeptName,
//            '#size' => 50,
//            '#disabled' => TRUE,
//        );
//        $form['detail_fieldset']['GoodTypeName'] = array(
//            '#type' => 'textfield',
//            '#title' => t('ประเภทรายการสินค้า'),
//            '#default_value' => $EMGoodType->GoodTypeName,
//            '#size' => 50,
//            '#disabled' => TRUE,
//        );
//        $form['detail_fieldset']['GoodGroupName'] = array(
//            '#type' => 'textfield',
//            '#title' => t('กลุ่มสินค้า'),
//            '#default_value' => $EMGoodGroup->GoodGroupName,
//            '#size' => 50,
//            '#disabled' => TRUE,
//        );



        $form['detail_fieldset']['GoodName1'] = array(
            '#type' => 'textfield',
            '#title' => t('รายการสินค้า'),
            '#default_value' => $EMGood->GoodName1,
            '#size' => 50,
            '#disabled' => TRUE,
        );

        $form['submit'] = array('#type' => 'submit',
            '#value' => t('บันทึก'));
//        $form['cancel'] = array(
//            '#type' => 'button',
//            '#value' => t('ยกเลิก'),
//            '#weight' => 20,
//            '#drupal_goto' => "show-branch",
//                //    '#submit' => array('mymodule_form_cancel1'),
//        );
        $form['clear'] = array(
            '#name' => 'clear',
            '#type' => 'button',
            '#value' => t('Reset'),
            '#attributes' => array('onclick' => 'this.form.reset(); return false;'),
        );
        return $form;
    }
}

function manage_min_stock_edit_form($form, $form_state, $listNo, $inveid, $brchid, $goodid, $deptid) {

    $EMInve = reset(entity_load('EMInve', array($inveid), array()));
    $EMBrch = reset(entity_load('EMBrch', array($brchid), array()));
    $EMGood = reset(entity_load('EMGood', array($goodid), array()));
    drupal_set_title("ตั้งค่า " . $EMGood->GoodName1);
    $EMDept = reset(entity_load('EMDept', array($deptid), array()));
    dd($EMGood);


    $sql_order = "SELECT ListNo,MinPoint,MaxPoint FROM EMGoodOrder where ListNo = $listNo ";
    $result_order = db_query($sql_order)->fetchAll();
    foreach ($result_order as $value_order) {
        $value_order->ListNo;
        $value_order->MinPoint;
        $value_order->MaxPoint;
    }
//    if ($result_order != NULL) {
    $form['edit_minpoint_fieldset'] = array(
        '#type' => 'fieldset',
        '#title' => t('แก้ไข ตั้งค่าจำนวนต่ำสุดของวัตถุดิบ'),
    );
    $form['edit_minpoint_fieldset']['MinPoint'] = array(
        '#type' => 'textfield',
        '#title' => t('ตั้งค่า MinPoint'),
        '#maxlength' => 64,
        '#size' => 15,
        '#default_value' => $value_order->MinPoint,
    );
    $form['edit_minpoint_fieldset']['MaxPoint'] = array(
        '#type' => 'textfield',
        '#title' => t('ตั้งค่า MaxPoint'),
        '#maxlength' => 64,
        '#size' => 15,
        '#default_value' => $value_order->MaxPoint,
    );

     
    $form['ListNo'] = array(
        '#type' => 'hidden',
        '#default_value' => $value_order->ListNo,
    );
    $form['BrchID'] = array(
        '#type' => 'hidden',
        '#default_value' => $brchid,
    );
    $form['GoodGroupID'] = array(
        '#type' => 'hidden',
        '#default_value' => $EMGood->GoodGroupID,
    );
     $form['detail_fieldset'] = array(
            '#type' => 'fieldset',
            '#title' => t('รายละเอียดการ ตั้งค่าจำนวนต่ำสุดของวัตถุดิบ'),
        );
    $form['detail_fieldset']['InveName'] = array(
        '#type' => 'textfield',
        '#title' => t('คลัง'),
        '#default_value' => $EMInve->InveName,
        '#size' => 50,
        '#disabled' => TRUE,
    );
    $form['detail_fieldset']['BrchName'] = array(
        '#type' => 'textfield',
        '#title' => t('สาขา'),
        '#default_value' => $EMBrch->BrchName,
        '#size' => 50,
        '#disabled' => TRUE,
    );
//    $form['detail_fieldset']['DeptID'] = array(
//        '#type' => 'textfield',
//        '#title' => t('แผนก'),
//        '#default_value' => $EMDept->DeptName,
//        '#size' => 50,
//        '#disabled' => TRUE,
//    );
    $form['detail_fieldset']['GoodID'] = array(
        '#type' => 'textfield',
        '#title' => t('รายการสินค้า'),
        '#default_value' => $EMGood->GoodName1,
        '#size' => 50,
        '#disabled' => TRUE,
    );


    $form['submit'] = array('#type' => 'submit',
        '#value' => t('บันทึก'));

    $form['clear'] = array(
        '#name' => 'clear',
        '#type' => 'button',
        '#value' => t('Reset'),
        '#attributes' => array('onclick' => 'this.form.reset(); return false;'),
    );
//}
    return $form;
}

function mymodule_form_cancel() {

    drupal_goto("check-stock");
}

function mymodule_form_cancel1() {
    drupal_goto('show-branch');
}

function get_maxID() {


    $sql = "select MAX(ListNo) as MAX_ID from EMGoodOrder ";

    $result = db_query($sql);

    $array = array();
    foreach ($result as $value) {

        $array[$value->MAX_ID] = (string) (intval($value->MAX_ID) + 1);
    }


    return $array;
}

function manage_min_stock_add_form_submit($form, $form_state) {

    $values = $form_state['values'];
    $table = 'EMGoodOrder';
    $BrchID = $values['BrchID'];
    $GoodGroupID = $values['GoodGroupID'];
    $check = $values['check_order'];

    if ($check == 1) {
        $maxList = get_maxID();
        foreach ($maxList as $value2) {
            
        }
        $entry = array(
            'ListNo' => $value2,
            'InveID' => $values['InveID'],
            'BrchID' => $values['BrchID'],
            'GoodID' => $values['GoodID'],
            'StartDate' => format_date(time(), 'custom', 'Ymd H:i:s'),
            'EndDate' => format_date(time(), 'custom', 'Y1231'),
            'MinPoint' => $values['MinPoint'],
            'DeptID' => '1000',
            'MaxPoint' => $values['MaxPoint'],
            'OrderPoint' => NULL,
            'LeadTime' => NULL,
            'MinUsed' => NULL,
            'MaxUsed' => NULL,
            'ReceQty' => NULL,
            'UsedQty' => NULL,
            'Remark' => NULL,
            'SafeQty' => NULL,
            'LotQty' => NULL,
        );

        $return_value = db_insert($table)
                ->fields($entry)
                ->execute();
        drupal_set_message("ตั้งค่าค่ารายการเสร็จสิ้น");
        drupal_goto("/check-stock-all/$BrchID/$GoodGroupID");
    } else {
        $entry = array(
            'MinPoint' => $values['MinPoint'],
            'MaxPoint' => $values['MaxPoint'],
        );
        $return_value = db_update($table)
                ->fields($entry)
                ->condition('ListNo', $values['ListNo'])
                ->execute();

        drupal_set_message("แก้ไขรายการเสร็จสิ้น");
        drupal_goto("/check-stock-all/$BrchID/$GoodGroupID");
    }
}

function manage_min_stock_edit_form_submit($form, $form_state) {

    $values = $form_state['values'];
    $table = 'EMGoodOrder';
    $BrchID = $values['BrchID'];
    $GoodGroupID = $values['GoodGroupID'];


    $entry = array(
        'MinPoint' => $values['MinPoint'],
         'MaxPoint' => $values['MaxPoint'],
    );
    $return_value = db_update($table)
            ->fields($entry)
            ->condition('ListNo', $values['ListNo'])
            ->execute();

    drupal_set_message("แก้ไขรายการเสร็จสิ้น");
    drupal_goto("check-stock/$BrchID/$GoodGroupID");
}

?>
