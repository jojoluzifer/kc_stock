<?php
function kc_cart_po_cart_form($form,$form_state){
	$user = kc_user_account_load();
//	$option_InveID = kc_cart_po_select_option_EMInve();

	$form['#tree'] = TRUE;
/*	$form['InveID'] = array(
			'#type' => 'select',
			'#title' => t('เลือกสาขา'),
			'#options' => $option_InveID,
			'#default_value' => isset($form_state['values']['InveID'])?$form_state['values']['InveID']:1000,
			//	'#description' => t('Set this to <em>Yes</em> if you would like this category to be selected by default.'),
	);
*/
//	if (isset($form_state['values']['InveID'])){
//		$form['InveID']['#disabled'] = TRUE;
//		$inve = $form_state['values']['InveID'];
//		$inve = 1000;
		$order_line = kc_cart_po_select_cart_podt($user->EmpID);
		//		dsm($order_line);
		if (isset($order_line))
			$InveID = $order_line[0]->InveID;
		$form['InveID'] = array('#type' => 'hidden', '#value' => $InveID);
		$form['OrderLine'] = array(
				'#type' => 'fieldset',
				'#title' => t('Contact settings'),
				//			'#weight' => 5,
				'#collapsible' => TRUE,
				'#collapsed' => FALSE,
		);
		$form['OrderLine']['table_start'] = array(
				'#markup' => '<table>
				<tr>
				<th>No.</th>
				<th>รหัสสินค้า</th>
				<th>ชื่อสินค้า</th>
				<th>คลัง</th>
				<th>หน่วยนับ</th>
				<th>จำนวน</th>
				</tr>',
		);
		$i=1;
		foreach ($order_line as $line){
			$unitName = kc_cart_po_select_EMGoodUnit($line->GoodUnitID2);
			//dsm($unitName);
			$form['OrderLine'][$i]['No'] = array(
					'#type' => 'item',
					//				'#title' => t('No'),
					'#markup' => '<tr><td>'.($i).'</td>',
			);
			$form['OrderLine'][$i]['GoodID'] = array(
					'#type' => 'textfield',
					//				'#title' => t('No'),
					//					'#markup' => '<td>'.$line->GoodID.'</td>',
					'#default_value' => $line->GoodID,
					//				'#required' => TRUE,
					'#size'=> 10,
					'#prefix' => '<td>',
					'#suffix' => '</td>',
					'#disabled' => TRUE
			);
			$form['OrderLine'][$i]['GoodName'] = array(
					'#type' => 'item',
					//				'#title' => t('No'),
					'#markup' => '<td>'.$line->GoodName.'</td>',
			);
			$form['OrderLine'][$i]['InveID'] = array(
					'#type' => 'textfield',
					
					'#default_value' => $line->InveID,
					'#disabled' => TRUE,
					'#size'=> 10,
					'#prefix' => '<td>',
					'#suffix' => '</td>',
			);
			$form['OrderLine'][$i]['GoodUnit'] = array(
					'#type' => 'item',
					'#markup' => '<td>'.$unitName['GoodUnitName'].'</td>',
			);
			$form['OrderLine'][$i]['GoodQty2'] = array(
					'#type' => 'textfield',
					//				'#title' => t('Subject'),
					'#default_value' => $line->GoodQty2,
					//				'#required' => TRUE,
					'#size'=> 10,
					'#prefix' => '<td>',
					'#suffix' => '</td><tr>',
			);
			$i++;
		}
		$form['OrderLine']['table_end'] = array(
				'#markup' => '</table>',
		);
		$form['Save'] = array(
				'#type' => 'submit',
				'#value' => t('บันทึก')
		);
		$form['Submit_po'] = array(
				'#type' => 'submit',
				'#value' => t('สร้าง PR'),
				'#submit' => array('kc_cart_po_cart_form_submit_PR'),
		);
/*	}else{
		$form['Submit_po'] = array(
				'#type' => 'submit',
				'#value' => t('เลือกสาขา'),
				'#submit' => array('kc_cart_po_cart_form_submit_select_invelid'),
		);
	}
	*/
	return $form;
}
function kc_cart_po_cart_form_submit($form,&$form_state){
	$form_state['rebuild'] = TRUE;
//	dsm($form_state);
	kc_cart_save_cart($form_state);
}
function kc_cart_po_cart_form_submit_PR($form,&$form_state){
//	dsm('kc_cart_po_kc_cart_po_cart_form_submit_create_PR');
	kc_cart_save_cart($form_state);
	$form_state['rebuild'] = TRUE;
//	$InveID = $form_state['values']['InveID'];
	drupal_goto('kc_cart/create_pr/');
}
function kc_cart_po_cart_form_submit_select_invelid($form,&$form_state){
	$form_state['rebuild'] = TRUE;
}
function kc_cart_save_cart($form_state){
	$user = kc_user_account_load();
	$empID = $user->EmpID;
	$InveID = $form_state['values']['InveID'];
	$orderLine = $form_state['values']['OrderLine'];
	foreach ($orderLine as $line){
		$line['AppvQty2'] = $line['GoodQty2'];
		$line['GoodStockQty'] = $line['GoodQty2'];
		$line['GoodRemaQty2'] = $line['GoodQty2'];
		$num_updated=db_update('Cart_PODT')
		->fields($line)
		->condition('GoodID',$line['GoodID'])
		->condition('InveID',$InveID)
		->condition('EmpID',$empID)
		->execute();
	}
}
function kc_cart_po_create_pr_form($form,&$form_state,$InveID=NULL){
	$user = kc_user_account_load();
	$empID = $user->EmpID;
	//dsm($form_state);
//	$form_state['values']['InveID'] = isset($form_state['values']['InveID'])?$form_state['values']['InveID']:$InveID;
/*	$option_InveID = kc_cart_po_select_option_EMInve();
	$form['InveID'] = array(
			'#type' => 'select',
			'#title' => t('เลือกสาขา'),
			'#options' => $option_InveID,
			'#default_value' => $form_state['values']['InveID'],
			//	'#description' => t('Set this to <em>Yes</em> if you would like this category to be selected by default.'),
	);
*/
//	if (isset($form_state['values']['InveID'])){
		//$option_VendorID = kc_cart_po_select_option_EMVendor();
		$i=1;
		$form['#tree']=TRUE;
		$option_VendorID = kc_cart_po_select_option_EMVendor();
		$option_Transp = kc_cart_po_select_option_EMTransp();
		$order_line = kc_cart_po_select_cart_podt($empID);
		$emp = kc_user_account_load();
		//dsm($emp);
		dsm($order_line);
		if (isset($order_line))
		$InveID = $order_line[0]->InveID;
		$form['InveID'] = array('#type' => 'hidden', '#value' => $InveID);
		//dsm($InveID);
		$entity_Inve = entity_load_single('EMInve',$InveID);
		$Inve_code = substr($entity_Inve->InveCode, -2);
		$DocuNo = _kc_cart_po_getNextDocuNo_POHD($Inve_code.'PR');
		$form['PR_HD1'] = array(
				'#type' => 'fieldset',
				'#title' => t('PR HD'),
				//			'#weight' => 5,
				'#collapsible' => TRUE,
				'#collapsed' => FALSE,
		);
		$form['PR_HD1']['table_start'] = array(
				'#markup' => '<table>
				<tr>',
				);
		$form['PR_HD1']['VendorID'] = array(
				'#type' => 'select',
				'#title' => t('ผู้ขาย'),
				'#options' => $option_VendorID,
				'#prefix' => '<td>',
				'#suffix' => '</td>',
				//	'#default_value' => $category['selected'],
		);
		$form['PR_HD1']['DocuNo'] = array(
				'#type' => 'textfield',
				'#title' => t('เลขที่เอกสาร'),
				'#default_value' => $DocuNo,
				'#disabled' => TRUE,
				'#prefix' => '<td>',
				'#suffix' => '</td></tr>',
				'#size'=> 25,
		);
		$form['PR_HD1']['DocuDate'] = array(
				'#type' => 'date',
				'#title' => 'วันที่เอกสาร',
				'#default_value' => array(
						'month' => t(format_date(time(), 'custom', 'n')),
						'day' => t(format_date(time(), 'custom', 'j')),
						'year' => format_date(time(), 'custom', 'Y'),
				),
				'#disabled' => TRUE,
				'#prefix' => '<tr><td>',
				'#suffix' => '</td>',
				'#size'=> 25,
		);
		$form['PR_HD1']['ReqInTime'] = array(
				'#type' => 'textfield',
				'#title' => t('ต้อกการภายใน (วัน)'),
				'#prefix' => '<td>',
				'#suffix' => '</td></tr>',
				'#size'=> 25,
		);
		$form['PR_HD1']['table_end'] = array(
				'#markup' => '</tr></table>',
		);
		
		$form['PR_HD2'] = array(
				'#type' => 'fieldset',
				'#title' => t('PR HD'),
				//			'#weight' => 5,
				'#collapsible' => TRUE,
				'#collapsed' => FALSE,
		);
		$form['PR_HD2']['table_start'] = array(
				'#markup' => '<table>
				<tr>',
		);
		$form['PR_HD2']['ShipDate'] = array(
				'#type' => 'date',
				'#title' => 'วันที่กำหนดส่ง',
				'#default_value' => array(
						'month' => t(format_date(time(), 'custom', 'n')),
						'day' => t(format_date(time(), 'custom', 'j')+1),
						'year' => format_date(time(), 'custom', 'Y'),
				),
				'#prefix' => '<td>',
				'#suffix' => '</td>',
	);
				
		$form['PR_HD2']['CrdtDays'] = array(
				'#type' => 'textfield',
				'#title' => t('เครดิต (วัน)'),
				'#size'=> 25,
				'#prefix' => '<td>',
				'#suffix' => '</td>',
		);
		$form['PR_HD2']['table_row1'] = array(
				'#markup' => '</tr><tr>',
		);
		$form['PR_HD2']['TranspID'] = array(
				'#type' => 'select',
				'#title' => t('ขนส่งโดย'),
				'#options' => $option_Transp,
				//	'#default_value' => $category['selected'],
				'#prefix' => '<td>',
				'#suffix' => '</td>',
		);
		
		
		$form['PR_HD2']['ReqbyID'] = array(
				'#type' => 'textfield',
				'#title' => t('ไอดี ผู้ขอชื้อ'),
				'#default_value' => $emp->EmpID,
				'#disabled' => TRUE,
				'#size'=> 25,
				'#prefix' => '<td>',
				'#suffix' => '</td>',
		);
		$form['PR_HD2']['table_row2'] = array(
				'#markup' => '</tr><tr>',
		);
		$form['PR_HD2']['ReqbyName'] = array(
				'#type' => 'textfield',
				'#title' => t('ผู้ขอซื้อ'),
				'#default_value' => $emp->EmpName,
				'#disabled' => TRUE,
				'#size'=> 25,
				'#prefix' => '<td>',
				'#suffix' => '</td>',
		);
		$form['PR_HD2']['table_end'] = array(
				'#markup' => '</tr></table>',
		);
		$form['OrderLine'] = array(
				'#type' => 'fieldset',
				'#title' => t('Contact settings'),
				//			'#weight' => 5,
				'#collapsible' => TRUE,
				'#collapsed' => FALSE,

		);
		$form['OrderLine']['table_start'] = array(
				'#markup' => '<table>
				<tr>
				<th>No.</th>
				<th>รหัสสินค้า</th>
				<th>ชื่อสินค้า</th>
				<th>คลัง</th>
				<th>หน่วยนับ</th>
				<th>จำนวน</th>
				</tr>',
		);
		foreach ($order_line as $line){
			$unitName = kc_cart_po_select_EMGoodUnit($line->GoodUnitID2);
			//dsm($unitName);
			$form['OrderLine'][$i]['No'] = array(
					'#type' => 'item',
					//				'#title' => t('No'),
					'#markup' => '<tr><td>'.($i).'</td>',
			);
			$form['OrderLine'][$i]['GoodID'] = array(
					'#type' => 'textfield',
					//				'#title' => t('No'),
					//					'#markup' => '<td>'.$line->GoodID.'</td>',
					'#default_value' => $line->GoodID,
					//				'#required' => TRUE,
					'#size'=> 10,
					'#prefix' => '<td>',
					'#suffix' => '</td>',
					'#disabled' => TRUE
			);
			$form['OrderLine'][$i]['GoodName'] = array(
					'#type' => 'item',
					//				'#title' => t('No'),
					'#markup' => '<td>'.$line->GoodName.'</td>',
			);
			$form['OrderLine'][$i]['Invel'] = array(
					'#type' => 'item',
					'#markup' => '<td>'.$line->InveID.'</td>',
			);
			$form['OrderLine'][$i]['GoodUnit'] = array(
					'#type' => 'item',
					'#markup' => '<td>'.$unitName['GoodUnitName'].'</td>',
			);
			$form['OrderLine'][$i]['GoodQty2'] = array(
					'#type' => 'textfield',
					//				'#title' => t('Subject'),
					'#default_value' => $line->GoodQty2,
					//				'#required' => TRUE,
					'#size'=> 10,
					'#prefix' => '<td>',
					'#suffix' => '</td><tr>',
			);
			$i++;
		}
		$form['OrderLine']['table_end'] = array(
				'#markup' => '</table>',
		);
		$form['OrderLine']['table_end'] = array(
				'#markup' => '</table>',
		);
		$form['Submit_po2'] = array(
				'#type' => 'submit',
				'#value' => t('ยืนยัน'),
				'#submit' => array('kc_cart_po_cart_form_submit_create_PR'),
		);

	return $form;
}
function kc_cart_po_cart_form_submit_create_PR($form,&$form_state){
	//	dsm('kc_cart_po_kc_cart_po_cart_form_submit_create_PR');
	$form_state['rebuild'] = TRUE;
	//	dsm($form_state);
	kc_cart_save_cart($form_state);
	$values = $form_state['values'];
	kc_cart_po_create_pr($values);
	$InveID = $values['InveID'];
	$entity_Inve = entity_load_single('EMInve',$InveID);
    drupal_goto('/view-pr-appvflag/'.$entity_Inve->InveCode);
}

function kc_cart_po_create_pr($values){

	//	$InveID = $values['InveID'];
	$order = kc_cart_po_create_pohd_by_pr_app($values);
	kc_cart_po_create_podt_by_pr_app($order,$values);
	//	if ($orderLine['GoodQty'])
}
function kc_cart_po_create_pohd_by_pr_app($values){
//s	dsm($values);
	$values_poht =array();
	$ret = db_query('select max(poid)+1 from POHD')->fetchCol();
	$entity_inve = entity_load_single('EMInve', $values['InveID']);
	$Inve_code = substr($entity_inve->InveCode, -2);
	$DocuDate = $values['PR_HD1']['DocuDate']['month'].'-'.$values['PR_HD1']['DocuDate']['day'].'-'.$values['PR_HD1']['DocuDate']['year'].' 0:0:0.000';
	$ShipDate =  $values['PR_HD2']['ShipDate']['month'].'-'.$values['PR_HD2']['ShipDate']['day'].'-'.$values['PR_HD2']['ShipDate']['year'].' 0:0:0.000';
	$AppvDocuNo = $DocuNo = _kc_cart_po_getNextDocuNo_POHD_av($Inve_code.'AV');




	$values_poht['POID'] = $ret[0];
	$values_poht['VendorID'] = $values['PR_HD1']['VendorID'];
	$values_poht['VATGroupID'] = 1; //7
	$values_poht['BrchID'] = $entity_inve->BrchID;
	$values_poht['DocuDate'] = $DocuDate;
	$values_poht['DocuNo'] = $values['PR_HD1']['DocuNo'];
	$values_poht['ReqbyID'] = $values['PR_HD2']['ReqbyID'];
	$values_poht['ReqInTime'] = $values['PR_HD1']['ReqInTime'];
	$values_poht['ShipDays'] = 0;
	$values_poht['CrdtDays'] = 0;
	$values_poht['ShipDate'] = $ShipDate;
	$values_poht['VATType'] = 1; //3
	$values_poht['VATRate'] = 7;
	$values_poht['GoodType'] = 1;
	$values_poht['ExchRate'] = 1;
	$values_poht['SumIncludeAmnt'] = 0;
	$values_poht['SumExcludeAmnt'] = 0;
	$values_poht['SumGoodAmnt'] = 0;
	$values_poht['BaseDiscAmnt'] = 0;
	$values_poht['BillDiscFormula'] = '';
	$values_poht['BillDiscAmnt'] = 0;
	$values_poht['TotaExcludeAmnt'] =0;
	$values_poht['TotabaseAmnt']= 0;
	$values_poht['VATAmnt'] = 0;
	$values_poht['NetAmnt']	=2;
	$values_poht['BillDiscType'] =2;
	$values_poht['DocuStatus'] = 'N';
	$values_poht['OnHold'] = 'N';
	$values_poht['DocuType'] = 301;
	$values_poht['BillChargAmnt'] = 0;
	$values_poht['BillAftrChargAmnt'] = 0;
	$values_poht['MultiCurr'] = 'N';
	$values_poht['DiscPayIntimeAmnt'] = 0;
	$values_poht['RefDocuNo'] = '';
	$values_poht['AppvByID'] = $values['PR_HD2']['ReqbyID'];
	$values_poht['AppvDate'] = $DocuDate;
	$values_poht['AppvFlag'] = 2;
	$values_poht['AppvDocuNo'] = $AppvDocuNo;
	$values_poht['CancelFlag'] = 'N';
	$values_poht['BillAftrDiscAmnt'] = 0;



	$values_poht['TranspID'] = $values['PR_HD2']['TranspID'];
	$values_poht['JobID'] = 1000;
	$values_poht['CreditID'] = NULL;
	$values_poht['BillToID'] = NULL;
	$values_poht['CurrID'] = NULL;
	$values_poht['CurrTypeID'] = NULL;


	$values_poht['DeptID'] =1000;

	//dsm($values_poht);
//	$sql = _arrayToSql('POHD', $values_poht);
//	dsm($sql);
	$pohd = entity_create('POHD', $values_poht);
	$pohd->save();
	return $values_poht;
}
function kc_cart_po_create_podt_by_pr_app($order,$values){
	dsm($order);
	dsm($values);
	$user = kc_user_account_load();
	$EmpID = $user->EmpID;
	$ListNo = 1;
	foreach ($values['OrderLine'] as $line){
		dsm($line);
		//$order_line = kc_cart_po_select_cart_podt_by_goodid($form_state['values']['InveID']);
		$values_podt = kc_cart_po_select_cart_podt($user->EmpID,$line['GoodID']);
		dsm($values_podt);
		$values_podt['POID'] = $order['POID'];
		$values_podt['ListNo'] = $ListNo;
		
		$values_podt['GoodStockRate2'] =1;
		$values_podt['AppvFlag'] = 2;
		$values_podt['CompleteFlag'] ='N';
		$values_podt['DropShipID'] = NULL;
		unset($values_podt['MinPoint']);
		unset($values_podt['MaxPoint']);
		unset($values_podt['GoodQty']);
		unset($values_podt['EmpID']);
		$sql = _arrayToSql('PODT', $values_podt);
		dsm($sql);
		$podt = entity_create('PODT', $values_podt);
		$podt->save();
		$ListNo++;
		//db_delete($table)
		kc_cart_po_delete_cart_podt($EmpID,$line['GoodID']);
	}
	
}

function _arrayToSql($table, $array, $insert = "INSERT INTO") {

	//Check if user wants to insert or update
	if ($insert != "UPDATE") {
		$insert = "INSERT INTO";
	}

	$columns = array();
	$data = array();

	foreach ( $array as $key => $value) {
		$columns[] = $key;
		if ($value != "") {
			$data[] = "'" . $value . "'";
		} else {
			$data[] = "NULL";
		}

		//TODO: ensure no commas are in the values
	}

	$cols = implode(",",$columns);
	$values = implode(",",$data);

	$sql = <<<EOSQL
  $insert `$table`
  ($cols)
  VALUES
  ($values)
EOSQL;
	return $sql;

}

?>