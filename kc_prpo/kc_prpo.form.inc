<?php

function kc_prpo_add_form($form, $form_state, $poid) {

    $POHD = reset(entity_load('POHD', array($poid), array()));
    $PODT = reset(entity_load('PODT', array($poid), array()));
    $Vendor = reset(entity_load('EMVendor', array($POHD->VendorID), array()));

    /*  $sql_inve = "SELECT InveID,InveCode,InveName from EMInve where BrchID = $POHD->BrchID ";
      $result_inve = db_query($sql_inve)->fetchAll();
      foreach ($result_inve as $value_inve) {
      $inve_id = $value_inve->InveID;
      //    $Inve_code = substr($value_inve->InveCode, -2);
      // $DocuNo = _kc_cart_po_getNextDocuNo_POHD($Inve_code . 'PO');
      }
     * */
    $inve_code = substr($POHD->AppvDocuNo, 0, -10);
    $DocuNo = _kc_cart_po_getNextDocuNo_POHD($inve_code . 'PO');

    $sql_podt = "SELECT POID ,ListNo ,InveID,GoodID ,GoodName ,GoodQty2,GoodUnitID2,GoodPrice2 from PODT where POID = $poid ";
    $result_podt = db_query($sql_podt)->fetchAll();

    drupal_set_title("อนุมัติใบสั่งซื้อ");

    $form['POID'] = array(
        '#type' => 'hidden',
        '#default_value' => $poid,
    );
    $form['DocuNo'] = array(
        '#type' => 'hidden',
        '#default_value' => $DocuNo,
    );
    $form['AppvDate'] = array(
        '#type' => 'hidden',
        '#default_value' => $POHD->AppvDate,
    );
    $form['index_inveCode'] = array(
        '#type' => 'hidden',
        '#default_value' => $inve_code,
    );
    $form['AppvDocuNo'] = array(
        '#type' => 'hidden',
        '#default_value' => $POHD->AppvDocuNo,
    );
    $form['AppvDate'] = array(
        '#type' => 'hidden',
        '#default_value' => $POHD->AppvDate,
    );
    $form['ShipDate'] = array(
        '#type' => 'hidden',
        '#default_value' => $POHD->ShipDate,
    );
    $form['BrchID'] = array(
        '#type' => 'hidden',
        '#default_value' => $POHD->BrchID,
    );
    /*
      dd("--------------><-------------------");
      dd("InveId : " . $inve_id);
      dd("DocuNo : " . $DocuNo);
      dd("AppvDate : " . $POHD->AppvDate);
      dd("ReqByID : " . $POHD->ReqByID);
      dd("AppvByID : " . $POHD->AppvByID);
      dd("--------------><-------------------");
     */

    $form['#tree'] = TRUE;
    $form['kc_prpo_add_fieldset'] = array(
        '#type' => 'fieldset',
        '#title' => t('รายละเอียด'),
        '#collapsible' => FALSE,
        '#collapsed' => FALSE,
    );
    $form['kc_prpo_add_fieldset']['DocuNo_add'] = array(
        '#type' => 'textfield',
        '#title' => t('เลขที่เอกสาร'),
        '#maxlength' => 64,
        '#size' => 15,
        '#default_value' => $DocuNo,
        '#disabled' => TRUE,
    );
    $form['kc_prpo_add_fieldset']['ReqInTime'] = array(
        '#type' => 'textfield',
        '#title' => t('ต้องการภายใน(วัน)'),
        '#maxlength' => 64,
        '#size' => 15,
        '#default_value' => $POHD->ReqInTime,
        '#disabled' => TRUE,
    );
    $form['kc_prpo_add_fieldset']['AppvDocuNo_add'] = array(
        '#type' => 'textfield',
        '#title' => t('Approve PR'),
        '#maxlength' => 64,
        '#size' => 15,
        '#default_value' => $POHD->AppvDocuNo,
        '#disabled' => TRUE,
    );
    $form['kc_prpo_add_fieldset']['date_add'] = array(
        '#type' => 'textfield',
        '#title' => t('วันที่เอกสาร'),
        '#maxlength' => 64,
        '#size' => 15,
        '#default_value' => date('d-m-Y'),
        '#disabled' => TRUE,
    );
    $form['kc_prpo_add_fieldset']['VendorCode_add'] = array(
        '#type' => 'textfield',
        '#title' => t('รหัสผู้ชาย'),
        //     '#maxlength' => 64,
        '#size' => 15,
        '#default_value' => $Vendor->VendorCode,
        '#disabled' => TRUE,
    );

//        $form['kc_prpo_add_fieldset']['VendorName_add'] = array(
//        '#type' => 'item',
//        '#title' => t('ชื่อผู้ชาย'),
//        '#markup' => $Vendor->VendorName,
//      //  '#disabled' => TRUE,
//    );
    $form['kc_prpo_add_fieldset']['VendorName_add'] = array(
        '#type' => 'textfield',
        '#title' => t('ชื่อผู้ชาย'),
        //     '#maxlength' => 64,
        '#size' => 25,
        '#default_value' => $Vendor->VendorName,
        '#disabled' => TRUE,
    );
    $form['kc_prpo_add_fieldset']['AppvRemark_POHD'] = array(
        '#type' => 'textfield',
        '#title' => t('หมายเหตุ'),
    );
//    $header = array('เลขที่เอกสาร','ต้องการภายใน(วัน)','Approve PR');
//    $rows = array(
//        array('11','22',  substr($Vendor->VendorName, 2,10))
//    );
//    $form['tableHD'] = array(
//    '#theme' => 'table',
//    '#header' => $header,
//    '#rows' => $rows,
//    '#empty' =>t('Your table is empty'),
//);
    $form_state['list'] = 5;
    $form['POList'] = array(
        '#type' => 'fieldset',
        '#title' => t('รายการสินค้า'),
        //			'#weight' => 5,
        '#collapsible' => FALSE,
        '#collapsed' => FALSE,
    );
    $form['POList']['table_start'] = array(
        '#markup' => '<table>
			<tr>
			<th>No.</th>
			<th>รหัสสินค้า</th>
			<th>ชื่อสินค้า</th>
			<th>คลัง</th>
			<th>หน่วยนับ</th>
			<th>จำนวน</th>
                        <th>ราคาต่อหน่วย</th>
                        <th>หมายเหตุ</th>
			</tr>',
    );
    $i = 0;
    foreach ($result_podt as $value_podt) {
        $EMGood = reset(entity_load('EMGood', array($value_podt->GoodID), array()));
        $InveCo = reset(entity_load('EMInve', array($value_podt->InveID), array()));
        $GoodUnit = reset(entity_load('EMGoodUnit', array($value_podt->GoodUnitID2), array()));
        $form['POList'][$i]['ListNo'] = array(
            '#type' => 'hidden',
            '#default_value' => $value_podt->ListNo,
        );
        $form['POList'][$i]['No'] = array(
            '#type' => 'item',
            //				'#title' => t('No'),
            '#markup' => '<tr><td>' . ($i + 1) . '</td>',
        );

        $form['POList'][$i]['GoodID'] = array(
            '#type' => 'item',
            //				'#title' => t('No'),
            '#markup' => '<td>' . $EMGood->GoodCode . '</td>',
        );
        $form['POList'][$i]['GoodName'] = array(
            '#type' => 'item',
            //				'#title' => t('No'),
            //				'#markup' => '<td>' . $value_podt->GoodName . '</td>'
            '#markup' => '<td>' . $value_podt->GoodName . '</td>',
        );
        $form['POList'][$i]['Invel'] = array(
            '#type' => 'item',
            '#markup' => '<td>' . $InveCo->InveCode . '</td>',
        );
        $form['POList'][$i]['GoodUnit'] = array(
            '#type' => 'item',
            '#markup' => '<td>' . $GoodUnit->GoodUnitName . '</td>',
        );
        $form['POList'][$i]['GoodQty2'] = array(
            '#type' => 'textfield',
            //				'#title' => t('Subject'),
            //				'#default_value' => $node->title,
            //				'#required' => TRUE,
            '#size' => 5,
            '#prefix' => '<td>',
            '#default_value' => $value_podt->GoodQty2,
            '#suffix' => '</td>',
        );
        $form['POList'][$i]['GoodPrice2'] = array(
            '#type' => 'textfield',
            //				'#title' => t('Subject'),
            //				'#default_value' => $node->title,
            //				'#required' => TRUE,
            '#size' => 5,
            '#prefix' => '<td>',
            '#default_value' => $value_podt->GoodPrice2,
            '#suffix' => '</td>',
        );
        $form['POList'][$i]['AppvRemark_PODT'] = array(
            '#type' => 'textfield',
            //				'#title' => t('Subject'),
            //				'#default_value' => $node->title,
            //				'#required' => TRUE,
            '#size' => 20,
            '#prefix' => '<td>',
            '#suffix' => '</td></tr>',
        );
        $i++;
    }
    $form['POList']['table_end'] = array(
        '#markup' => "</table>",
    );
    $form['Submit_po'] = array(
        '#type' => 'submit',
        '#value' => t('อนุมัติ'),
        '#submit' => array('kc_po_approve_po'),
    );
//    $form['Submit_po2'] = array(
//        '#type' => 'submit',
//        '#value' => t('ไม่อนุมัติ'),
//        '#submit' => array('kc_po_not_approve_po'),
//    );


    return $form;
}

function kc_po_approve_po($form, &$form_state) {
    $sum_goodAmnt = 0;
    $sum_totaBaseAmnt = 0;
    $sum_vat = 0;
    $sum_netamnt = 0;
    $values = $form_state['values'];
  
    $ret_pohd = db_query('select max(poid)+1 from POHD')->fetchCol();

    /* dd("------------------------------------------------------------------- ");
      dd("---------> DocuNo : " . $values['DocuNo']);
      dd("---------> AppvDocuNo : " . $values['AppvDocuNo']);
     */
    foreach ($values['POList'] as $line1) {
        $sum_goodAmnt = ($line1['GoodQty2'] * $line1['GoodPrice2'] );
        $sum_totaBaseAmnt += $sum_goodAmnt;
        //      dd("sum GoodAmnt : " . $sum_goodAmnt); // รวม จำนวน * ราคาต่อหน่วย
    }
    $sum_vat = ($sum_totaBaseAmnt * 7 ) / 100;
    $sum_netamnt = $sum_totaBaseAmnt + $sum_vat;
    /*  dd("TotabaseAmnt : " . $sum_totaBaseAmnt); // รวมเงินทั้งสิน
      dd(" VatAmnt : " . $sum_vat); // ภาษี
      dd(" NetAmnt : " . $sum_netamnt); //รวมเงินทั้งสิน
     */

    $Inve_code = substr($values['index_inveCode'], -2);
    $AppvDocuNo_PA = _kc_cart_po_getNextDocuNo_POHD_av($Inve_code . 'PA');
   // dd($AppvDocuNo_PA);

    $array_poht = db_select('POHD', 'n')
            ->fields('n')
            ->condition('POID', $values['POID'])
            ->execute()
            ->fetchAssoc();
    $array_poht['POID'] = $ret_pohd[0];
    $array_poht['VATGroupID'] = '1'; // ตั้งค่า VAT เป็น Default มีค่า VatRate 7 Vattype 1
    $array_poht['DocuNo'] = $values['DocuNo'];
    // $array_poht['ShipDate'] = date("Y-m-d"); // วันที่ต้องส่งของ   จะเอาจาก ReqInTime มา + กับ  ShipDate แล้วจะได้ระยะวันส่งของ
    $array_poht['VATType'] = '1';
    $array_poht['VATRate'] = '7';
    $array_poht['SumExcludeAmnt'] = $sum_totaBaseAmnt; //จำนวนเงินรวม
    $array_poht['SumGoodAmnt'] = $sum_totaBaseAmnt; //จำนวนเงินรวม
    $array_poht['BaseDiscAmnt'] = ""; //ส่วนลด %
    $array_poht['BillDiscFormula'] = "";   //ส่วนลด %
    $array_poht['BillDiscAmnt'] = "";   //ส่วนลด %
    $array_poht['TotabaseAmnt'] = $sum_totaBaseAmnt;
    $array_poht['VATAmnt'] = $sum_vat; //ค่าภาษี Vat
    $array_poht['NetAmnt'] = $sum_netamnt; //รวมเงินส่วนลด
    $array_poht['DocuType'] = "305";
    $array_poht['AppvByID'] = "1005";
    $array_poht['RefDocuDate'] = $values['AppvDate'];
    $array_poht['RefDocuNo'] = $values['AppvDocuNo'];
    $array_poht['AppvDate'] = date("Y-m-d 00:00:00");
    $array_poht['AppvDocuNo'] = $AppvDocuNo_PA;
    $array_poht['AppvRemark'] = $values['kc_prpo_add_fieldset']['AppvRemark_POHD'];
    $array_poht['BillAftrDiscAmnt'] = $sum_netamnt; //รวมเงินส่วนลด
    $array_poht['PRReqByID'] = "1005";

    $pohd_New = entity_create('POHD', $array_poht);
    $pohd_New->save();
  //  dsm($array_poht);
    foreach ($values['POList'] as $line) {
        
        dd($line['AppvRemark_PODT']);
        $array_podt = db_select('PODT', 'nn')
                ->fields('nn')
                ->condition('POID', $values['POID'])
                ->condition('ListNo', $line['ListNo'])
                ->execute()
                ->fetchAssoc();
        $array_podt['POID'] = $ret_pohd[0];
        $array_podt['GoodRemaQty1'] = $line['GoodQty2'];
        $array_podt['GoodQty2'] = $line['GoodQty2'];
        $array_podt['GoodPrice2'] = $line['GoodPrice2'];
        $array_podt['AppvGoodAmnt'] = $sum_goodAmnt;
        $array_podt['GoodRemaQty2'] = $line['GoodQty2'];
        $array_podt['GoodAmnt'] = $sum_goodAmnt;
        $array_podt['DocuType'] = '305';
        $array_podt['RefPOID'] = $values['POID'];
        $array_podt['AppvRemark'] = $line['AppvRemark_PODT'];
        $array_podt['RefListNo'] = $line['ListNo'];
        $array_podt['RemaGoodStockQty'] = $line['GoodQty2'];
        $array_podt['remaamnt'] = $sum_goodAmnt;
        $array_podt['ShipDate'] = $values['ShipDate'];

        $array_podt['GoodStockRate2'] = 1;
        $array_podt['AppvFlag'] = 2;
        $array_podt['CompleteFlag'] = 'N';

        $array_podt['DropShipID'] = NULL;

        //   dsm($array_podt);

        $array_podt['DropShipID'] = $array_podt['InveID'];
        
        dsm($array_podt);
        $podt_New = entity_create('PODT', $array_podt);
        $podt_New->save();

    }
    $form_state['rebuild'] = TRUE;
    drupal_goto('view-po-appvflag/' . $values['BrchID']);
}

function kc_po_not_approve_po($form, &$form_state) {
    dsm('kc_po_not_approve_po');
    dsm($form);
    dsm($form_state);
    $form_state['rebuild'] = TRUE;
}

?>
