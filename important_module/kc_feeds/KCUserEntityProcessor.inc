<?php

/**
 * @file
 * FeedsUserProcessor class.
 */

/**
 * Feeds processor plugin. Create users from feed items.
 */
class KCUserEntityProcessor extends FeedsProcessor {
	/**
	 * Define entity type.
	 */
	public function entityType() {
		return $this->config['target_entity'];
	}

	/**
	 * Implements parent::entityInfo().
	 */
	protected function entityInfo() {
		$info = parent::entityInfo();
		$info['label plural'] = $this->entityType();
		return $info;
	}

	/**
	 * Creates a new user account in memory and returns it.
	 */
	protected function newEntity(FeedsSource $source) {
		$entity = entity_create($this->entityType(), $this->config['values']);
			
		return $entity;
	}

	/**
	 * Loads an existing user.
	 */
	protected function entityLoad(FeedsSource $source, $entity_id) {
		$result = entity_load(array($entity_id));
		return reset($result);
	}


	/**
	 * Save a user account.
	 */
	protected function entitySave($account) {

		if ($this->config['defuse_mail']) {
			$account->mail = $account->mail . '_test';
		}
		$account->status=$this->config['status'];
		$edit = (array) $account;

		// Remove pass from $edit if the password is unchanged.
		if (isset($account->feeds_original_pass) && $account->pass == $account->feeds_original_pass) {
			unset($edit['pass']);
		}

		user_save($account, $edit);
		if ($account->uid && !empty($account->openid)) {
			$authmap = array(
					'uid' => $account->uid,
					'module' => 'openid',
					'authname' => $account->openid,
			);
			if (SAVED_UPDATED != drupal_write_record('authmap', $authmap, array('uid', 'module'))) {
				drupal_write_record('authmap', $authmap);
			}
		}
		//		dsm($edit);
		$input_role->name=$edit['roles'];
		if ($input_role->name==NULL){
			$input_role->name='authenticated user';
		}
		$rid=user_role_load_by_name($input_role->name);
		if ($rid==''){
			$a=user_role_save($input_role);
		}
		$role= user_role_load_by_name($input_role->name);
		$rid = $role->rid;
		$role_name = $role->name;
		$roles = $account->roles + array($rid => $role_name,);
		user_save($account, array('roles' => $roles));

	}

	/**
	 * Delete multiple user accounts.
	 */
	protected function entityDeleteMultiple($ids) {
		entity_delete_multiple($ids);
	}

	/**
	 * Override parent::configDefaults().
	 */
	public function configDefaults() {
		return array(
				'target_entity'=> $this->entityType(),
				'values' => array(),
				'status' => 1,
		) + parent::configDefaults();
	}



	/**
	 * Override parent::configForm().
	 */
	public function configForm(&$form_state) {
		$form = parent::configForm($form_state);
		$form['status'] = array(
				'#type' => 'radios',
				'#title' => t('Status'),
				'#description' => t('Select whether users should be imported active or blocked.'),
				'#options' => array(0 => t('Blocked'), 1 => t('Active')),
				'#default_value' => $this->config['status'],
		);
		$form['values']['#tree'] = TRUE;
		$form['input_format']['#description'] = t('Select the input format for the %entity to be created.', array('%entity' => $this->entityType()));


		$entities = entity_get_info();
		$entity_options = array();
		foreach($entities as $name =>$entity){
			$entity_options[$name] = $entity['label'];
		}

		$form['target_entity'] = array(
				'#type' => 'select',
				'#title' => t('Entity'),
				'#description' => t('Select the entity type target to inport'),
				'#options' => $entity_options,
				'#default_value'=>$this->config['target_entity'],
				'#required' => TRUE,
		);

		/*
		 $wrapper = entity_metadata_wrapper($this->entityType());
		foreach ($wrapper->getPropertyInfo() as $name => $property) {
		if (!empty($property['required'])) {
		$form['values'][$name] = array(
				'#type' => 'textfield',
				'#title' => $property['label'],
				'#description' => isset($property['description']) ? $property['description'] : '',
				'#default_value' => isset($this->config['values'][$name]) ? $this->config['values'][$name] : NULL,
				'#required' => TRUE,
		);
		if (!empty($property['options list'])) {
		$form['values'][$name]['#type'] = 'select';
		$form['values'][$name]['#options'] = $wrapper->$name->optionsList();
		}
		// @todo: Maybe implement per data-type forms like Rules does?
		$form['values'][$name]['#description'] .= ' ' . t('Expected data type: %type.', array('%type' => $wrapper->$name->type()));
		if ($wrapper->$name instanceof EntityDrupalWrapper) {
		$info = $wrapper->$name->entityInfo();
		$id_info = $wrapper->$name->get($info['entity keys']['id'])->info();
		$form['values'][$name]['#description'] .= ' ' . t('Just enter the identifier of the entity, i.e. %id', array('%id' => $id_info['label']));
		}
		}
		}
		*/


		return $form;
	}


	public function configFormSubmit(&$values) {
		parent::configFormSubmit($values);
	}

	/**
	 * Return available mapping targets.
	 */
	public function getMappingTargets() {

		// Get a wrapper with the right bundle info.
		$entity_info = $this->entityInfo();
		$info = array('bundle' => NULL);

		if (isset($entity_info['entity keys']['bundle']) && isset($this->config['values'][$entity_info['entity keys']['bundle']])) {
			$info['bundle'] = $this->config['values'][$entity_info['entity keys']['bundle']];
		}
		else {
			$info['bundle'] = $this->entityType();
		}

		$wrapper = entity_metadata_wrapper($this->entityType(), NULL, $info);

		// 	@todo: maybe restrict to data types feeds can deal with.
		foreach ($wrapper->getPropertyInfo() as $name => $property) {
			//dsm($name);
			//if (!empty($property['setter callback'])) {
			$targets[$name] = array(
					'name' => $property['label'],
					'description' => isset($property['description']) ? $property['description'] : NULL,
			);
			//}
		}

		if(isset($targets[$entity_info['entity keys']['id']]))
			$targets[$entity_info['entity keys']['id']]['optional_unique'] = TRUE;
		else{
			$targets[drupal_strtoupper($entity_info['entity keys']['id'])]['optional_unique'] = TRUE;
		}
		$targets += array(
				'pass' => array(
						'name' => t('Unencrypted Password'),
						'description' => t('The unencrypted user password.'),
				),);

		// Let other modules expose mapping targets.
		self::loadMappers();
		$type = $this->entityType();
		drupal_alter('feeds_processor_targets', $targets, $type, $info['bundle']);

		return $targets;

	}

	/**
	 * Get id of an existing feed item term if available.
	 */
	/*
	 protected function existingEntityId(FeedsSource $source, FeedsParserResult $result) {
	if ($uid = parent::existingEntityId($source, $result)) {
	return $uid;
	}

	// Iterate through all unique targets and try to find a user for the
	// target's value.
	foreach ($this->uniqueTargets($source, $result) as $target => $value) {
	switch ($target) {
	case 'name':
	$uid = db_query("SELECT uid FROM {users} WHERE name = :name", array(':name' => $value))->fetchField();
	break;
	case 'mail':
	$uid = db_query("SELECT uid FROM {users} WHERE mail = :mail", array(':mail' => $value))->fetchField();
	break;
	case 'openid':
	$uid = db_query("SELECT uid FROM {authmap} WHERE authname = :authname AND module = 'openid'", array(':authname' => $value))->fetchField();
	break;
	}
	if ($uid) {
	// Return with the first nid found.
	return $uid;
	}
	}
		
	return 0;
	}
	*/


	/**
	 * Process the result of the parsing stage.
	 *
	 * @param FeedsSource $source
	 *   Source information about this import.
	 * @param FeedsParserResult $parser_result
	 *   The result of the parsing stage.
	 */
	public function process(FeedsSource $source, FeedsParserResult $parser_result) {
		parent::process($source, $parser_result);



		/*
		 $state = $source->state(FEEDS_PROCESS);

		while ($item = $parser_result->shiftItem()) {
		if (!($entity_id = $this->existingEntityId($source, $parser_result)) ||
				($this->config['update_existing'] != FEEDS_SKIP_EXISTING)) {

		// Only proceed if item has actually changed.
		$hash = $this->hash($item);
		if (!empty($entity_id) && $hash == $this->getHash($entity_id)) {
		continue;
		}

		try {
		// Assemble node, map item to it, save.
		if (empty($entity_id)) {
		$entity = $this->newEntity($source);
		$this->newItemInfo($entity, $source->feed_nid, $hash);
		}
		else {
		$entity = $this->entityLoad($source, $entity_id);
		// The feeds_item table is always updated with the info for the most recently processed entity.
		// The only carryover is the entity_id.
		$this->newItemInfo($entity, $source->feed_nid, $hash);
		$entity->feeds_item->entity_id = $entity_id;
		}

		$this->map($source, $parser_result, $entity);
		$this->entityValidate($entity);
		$this->entitySave($entity);

		// Track progress.
		if (empty($entity_id)) {
		$state->created++;
		}
		else {
		$state->updated++;
		}
		}
		catch (Exception $e) {
		$state->failed++;
		drupal_set_message($e->getMessage(), 'warning');
		$message = $e->getMessage();
		$message .= '<h3>Original item</h3>';
		$message .= '<pre>' . var_export($item, TRUE) . '</pre>';
		$message .= '<h3>Entity</h3>';
		$message .= '<pre>' . var_export($entity, TRUE) . '</pre>';
		$source->log('import', $message, array(), WATCHDOG_ERROR);
		}
		}
		}

		// Set messages if we're done.
		if ($source->progressImporting() != FEEDS_BATCH_COMPLETE) {
		return;
		}
		$info = $this->entityInfo();
		$tokens = array(
				'@entity' => strtolower($info['label']),
				'@entities' => strtolower($info['label plural']),
		);
		$messages = array();
		if ($state->created) {
		$messages[] = array(
				'message' => format_plural(
						$state->created,
						'Created @number @entity.',
						'Created @number @entities.',
						array('@number' => $state->created) + $tokens
				),
		);
		}
		if ($state->updated) {
		$messages[] = array(
				'message' => format_plural(
						$state->updated,
						'Updated @number @entity.',
						'Updated @number @entities.',
						array('@number' => $state->updated) + $tokens
				),
		);
		}
		if ($state->failed) {
		$messages[] = array(
				'message' => format_plural(
						$state->failed,
						'Failed importing @number @entity.',
						'Failed importing @number @entities.',
						array('@number' => $state->failed) + $tokens
				),
				'level' => WATCHDOG_ERROR,
		);
		}
		if (empty($messages)) {
		$messages[] = array(
				'message' => t('There are no new @entities.', array('@entities' => strtolower($info['label plural']))),
		);
		}
		foreach ($messages as $message) {
		drupal_set_message($message['message']);
		$source->log('import', $message['message'], array(), isset($message['level']) ? $message['level'] : WATCHDOG_INFO);
		}

		*/
	}



}
