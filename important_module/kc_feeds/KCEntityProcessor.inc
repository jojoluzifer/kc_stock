<?php

/**
 * @file
 * FeedsUserProcessor class.
 */

/**
 * Feeds processor plugin. Create users from feed items.
 */
class KCEntityProcessor extends FeedsProcessor {
	/**
	 * Define entity type.
	 */
	public function entityType() {
		return $this->config['target_entity'];
	}

	/**
	 * Implements parent::entityInfo().
	 */
	protected function entityInfo() {
		$info = parent::entityInfo();
		$info['label plural'] = $this->entityType();
		return $info;
	}

	/**
	 * Creates a new user account in memory and returns it.
	 */
	protected function newEntity(FeedsSource $source) {
		$entity = entity_create($this->entityType(), $this->config['values']);

		return $entity;
	}

	/**
	 * Loads an existing user.
	 */
	protected function entityLoad(FeedsSource $source, $entity_id) {
		$result = entity_load(array($entity_id));
		dsm($result);
		return reset($result);
	}


	/**
	 * Save a user account.
	 */
	protected function entitySave($entity) {
		if (isset($entity->GoodID)&& $entity->GoodID !=0){
			$GoodId = $entity->GoodID;
			$list_No = $entity->ListNo;
			$data = array();
			$data['ListNo'] = $entity->ListNo;
			$data['InveID'] = $entity->InveID;
			$data['BrchID'] = $entity->BrchID;
			$data['GoodID'] = $entity->GoodID;
			$data['DeptID'] = $entity->DeptID;
			$data['MaxPoint'] = $entity->MaxPoint;
			$data['MinPoint'] = $entity->MinPoint;
			$goodorder = db_select('EMGoodOrder','l')
			->fields('l')
			->condition('l.InveID',$data['InveID'])
			->condition('l.BrchID',$data['BrchID'])
			->condition('l.GoodID',$data['GoodID'])
			->execute()->fetchAssoc();

			if (isset($goodorder['ListNo'])){
				$data['ListNo'] = $goodorder['ListNo'];
				$num_updated = db_update('EMGoodOrder') // Table name no longer needs {}
				->fields($data)
				->condition('ListNo', $data['ListNo'],'=')
				->condition('GoodID', $data['GoodID'],'=')
				->execute();

			}else{
				$id = db_query("SELECT MAX(ListNo) as max_id FROM EMGoodOrder WHERE GoodID ='".$GoodId."'")->fetchCol();
				if ($id[0]<1){
					$ListNo = 1;
				}else {
					$ListNo = $id[0]+1;
				}
				$data['ListNo'] = $ListNo;
				$nid = db_insert('EMGoodOrder') // Table name no longer needs {}
				->fields($data)
				->execute();
			}
		}else{
			drupal_set_message('ไฟล์ที่ Import มีปัญหา กรุณา Import ใหม่อีกครั้ง');
			return 0;
		}
	}

	/**
	 * Delete multiple user accounts.
	 */
	protected function entityDeleteMultiple($ids) {
		entity_delete_multiple($ids);
	}

	/**
	 * Override parent::configDefaults().
	 */
	public function configDefaults() {
		return array(
				'target_entity'=> $this->entityType(),
				'values' => array(),
				'primary_keys' => '',
		) + parent::configDefaults();
	}



	/**
	 * Override parent::configForm().
	 */
	public function configForm(&$form_state) {
		$form = parent::configForm($form_state);
		$form['values']['#tree'] = TRUE;
		$form['input_format']['#description'] = t('Select the input format for the %entity to be created.', array('%entity' => $this->entityType()));


		$entities = entity_get_info();
		$entity_options = array();
		foreach($entities as $name =>$entity){
			$entity_options[$name] = $entity['label'];
		}

		$form['target_entity'] = array(
				'#type' => 'select',
				'#title' => t('Entity'),
				'#description' => t('Select the entity type target to inport'),
				'#options' => $entity_options,
				'#default_value'=>$this->config['target_entity'],
				'#required' => TRUE,
		);

		$form['primary_keys'] = array(
				'#type' => 'textfield',
				'#title' => t('Entity'),
				'#description' => t('Select the entity type target to inport'),
				//'#options' => $entity_options,
				'#default_value'=>$this->config['primary_keys'],
				'#required' => TRUE,
		);

		/*
		 $wrapper = entity_metadata_wrapper($this->entityType());
		foreach ($wrapper->getPropertyInfo() as $name => $property) {
		if (!empty($property['required'])) {
		$form['values'][$name] = array(
				'#type' => 'textfield',
				'#title' => $property['label'],
				'#description' => isset($property['description']) ? $property['description'] : '',
				'#default_value' => isset($this->config['values'][$name]) ? $this->config['values'][$name] : NULL,
				'#required' => TRUE,
		);
		if (!empty($property['options list'])) {
		$form['values'][$name]['#type'] = 'select';
		$form['values'][$name]['#options'] = $wrapper->$name->optionsList();
		}
		// @todo: Maybe implement per data-type forms like Rules does?
		$form['values'][$name]['#description'] .= ' ' . t('Expected data type: %type.', array('%type' => $wrapper->$name->type()));
		if ($wrapper->$name instanceof EntityDrupalWrapper) {
		$info = $wrapper->$name->entityInfo();
		$id_info = $wrapper->$name->get($info['entity keys']['id'])->info();
		$form['values'][$name]['#description'] .= ' ' . t('Just enter the identifier of the entity, i.e. %id', array('%id' => $id_info['label']));
		}
		}
		}
		*/


		return $form;
	}


	public function configFormSubmit(&$values) {
		parent::configFormSubmit($values);
	}

	/**
	 * Return available mapping targets.
	 */
	public function getMappingTargets() {

		// Get a wrapper with the right bundle info.
		$entity_info = $this->entityInfo();
		$info = array('bundle' => NULL);

		if (isset($entity_info['entity keys']['bundle']) && isset($this->config['values'][$entity_info['entity keys']['bundle']])) {
			$info['bundle'] = $this->config['values'][$entity_info['entity keys']['bundle']];
		}
		else {
			$info['bundle'] = $this->entityType();
		}

		$wrapper = entity_metadata_wrapper($this->entityType(), NULL, $info);

		// 	@todo: maybe restrict to data types feeds can deal with.
		foreach ($wrapper->getPropertyInfo() as $name => $property) {
			//dsm($name);
			//if (!empty($property['setter callback'])) {
			$targets[$name] = array(
					'name' => $property['label'],
					'description' => isset($property['description']) ? $property['description'] : NULL,
			);
			//}
		}

		if(isset($targets[$entity_info['entity keys']['id']]))
			$targets[$entity_info['entity keys']['id']]['optional_unique'] = TRUE;
		else{
			$targets[drupal_strtoupper($entity_info['entity keys']['id'])]['optional_unique'] = TRUE;
		}

		// Let other modules expose mapping targets.
		self::loadMappers();
		$type = $this->entityType();
		drupal_alter('feeds_processor_targets', $targets, $type, $info['bundle']);

		return $targets;

	}

	/**
	 * Get id of an existing feed item term if available.
	 */
	/*
	 protected function existingEntityId(FeedsSource $source, FeedsParserResult $result) {
	if ($uid = parent::existingEntityId($source, $result)) {
	return $uid;
	}

	// Iterate through all unique targets and try to find a user for the
	// target's value.
	/*
	foreach ($this->uniqueTargets($source, $result) as $target => $value) {
	switch ($target) {
	case 'name':
	$uid = db_query("SELECT uid FROM {users} WHERE name = :name", array(':name' => $value))->fetchField();
	break;
	case 'mail':
	$uid = db_query("SELECT uid FROM {users} WHERE mail = :mail", array(':mail' => $value))->fetchField();
	break;
	case 'openid':
	$uid = db_query("SELECT uid FROM {authmap} WHERE authname = :authname AND module = 'openid'", array(':authname' => $value))->fetchField();
	break;
	}
	if ($uid) {
	// Return with the first nid found.
	return $uid;
	}
	}
	*//*
	$e = reset(entity_load('',FALSE,array('ListNo'=> 000)));
	if($e){
	return
	}

	return 0;
	}



	/**
	* Process the result of the parsing stage.
	*
	* @param FeedsSource $source
	*   Source information about this import.
	* @param FeedsParserResult $parser_result
	*   The result of the parsing stage.
	*/
	public function process(FeedsSource $source, FeedsParserResult $parser_result) {
		parent::process($source, $parser_result);



		/*
		 $state = $source->state(FEEDS_PROCESS);

		while ($item = $parser_result->shiftItem()) {
		if (!($entity_id = $this->existingEntityId($source, $parser_result)) ||
				($this->config['update_existing'] != FEEDS_SKIP_EXISTING)) {

		// Only proceed if item has actually changed.
		$hash = $this->hash($item);
		if (!empty($entity_id) && $hash == $this->getHash($entity_id)) {
		continue;
		}

		try {
		// Assemble node, map item to it, save.
		if (empty($entity_id)) {
		$entity = $this->newEntity($source);
		$this->newItemInfo($entity, $source->feed_nid, $hash);
		}
		else {
		$entity = $this->entityLoad($source, $entity_id);
		// The feeds_item table is always updated with the info for the most recently processed entity.
		// The only carryover is the entity_id.
		$this->newItemInfo($entity, $source->feed_nid, $hash);
		$entity->feeds_item->entity_id = $entity_id;
		}

		$this->map($source, $parser_result, $entity);
		$this->entityValidate($entity);
		$this->entitySave($entity);

		// Track progress.
		if (empty($entity_id)) {
		$state->created++;
		}
		else {
		$state->updated++;
		}
		}
		catch (Exception $e) {
		$state->failed++;
		drupal_set_message($e->getMessage(), 'warning');
		$message = $e->getMessage();
		$message .= '<h3>Original item</h3>';
		$message .= '<pre>' . var_export($item, TRUE) . '</pre>';
		$message .= '<h3>Entity</h3>';
		$message .= '<pre>' . var_export($entity, TRUE) . '</pre>';
		$source->log('import', $message, array(), WATCHDOG_ERROR);
		}
		}
		}

		// Set messages if we're done.
		if ($source->progressImporting() != FEEDS_BATCH_COMPLETE) {
		return;
		}
		$info = $this->entityInfo();
		$tokens = array(
				'@entity' => strtolower($info['label']),
				'@entities' => strtolower($info['label plural']),
		);
		$messages = array();
		if ($state->created) {
		$messages[] = array(
				'message' => format_plural(
						$state->created,
						'Created @number @entity.',
						'Created @number @entities.',
						array('@number' => $state->created) + $tokens
				),
		);
		}
		if ($state->updated) {
		$messages[] = array(
				'message' => format_plural(
						$state->updated,
						'Updated @number @entity.',
						'Updated @number @entities.',
						array('@number' => $state->updated) + $tokens
				),
		);
		}
		if ($state->failed) {
		$messages[] = array(
				'message' => format_plural(
						$state->failed,
						'Failed importing @number @entity.',
						'Failed importing @number @entities.',
						array('@number' => $state->failed) + $tokens
				),
				'level' => WATCHDOG_ERROR,
		);
		}
		if (empty($messages)) {
		$messages[] = array(
				'message' => t('There are no new @entities.', array('@entities' => strtolower($info['label plural']))),
		);
		}
		foreach ($messages as $message) {
		drupal_set_message($message['message']);
		$source->log('import', $message['message'], array(), isset($message['level']) ? $message['level'] : WATCHDOG_INFO);
		}

		*/
	}



}
