﻿<?php

//$kc_value['SOID']='';
//$kc_value['']


function _kc_restws_order_resource_definition() {
	$definition = array(
			'order' => array(
					'create' => array(
							'help' => 'Creates a order',
							'callback' => '_order_resource_create',
							'file' => array('type' => 'inc', 'module' => 'kc_restws', 'name' => 'resources/order_resource'),
							'access arguments' => array('access content'),
							'access arguments append' => FALSE,
							'args' => array(
									array(
											'name' => 'order',
											'type' => 'array',
											'description' => 'The order object',
											'source' => 'data',
											'optional' => FALSE,
									),
							),
					),
			),
	);

	return $definition;
}


function _arrayToSql($table, $array, $insert = "INSERT INTO") {

	//Check if user wants to insert or update
	if ($insert != "UPDATE") {
		$insert = "INSERT INTO";
	}

	$columns = array();
	$data = array();

	foreach ( $array as $key => $value) {
		$columns[] = $key;
		if ($value != "") {
			$data[] = "'" . $value . "'";
		} else {
			$data[] = "NULL";
		}

		//TODO: ensure no commas are in the values
	}

	$cols = implode(",",$columns);
	$values = implode(",",$data);

	$sql = <<<EOSQL
  $insert `$table`
  ($cols)
  VALUES
  ($values)
EOSQL;
	return $sql;

}

function _order_resource_create_sohd($emcust, $order, $jobid){
	$ret = db_query('select max(soid)+1 from SOHD')->fetchCol();
	$soid = $ret[0];
	$docu_no = _kc_restws_getNextDocuNo($order['DocuNo']);
	$NetAmnt = $order['NetAmnt'];
	$totalBaseAmnt = ($NetAmnt* 100 / 107);
	$vat = $NetAmnt - $totalBaseAmnt;
	$order['SOID'] = $soid;
	$order['DocuNo']= $docu_no;
	/*	if (!(isset($emcust['CustID']))){
		$emcust['CustID'] = $order['CustID'];
	$emcust['CustName'] = $order['CustName'];
	}
	*/
	//$SaleAreaID = _order_resource_set_SaleAreaID($emcust['CustGroupID']);

	$deposit = $order['DownPay'];
	$deposit_txt = $order['DownPayText'];

	if ($deposit=="true"){
		$deposit_show = 'มัดจำ เป็นจำนวนเงิน = '.$deposit_txt;
	}else{
		$deposit_show = 'ไม่ได้มัดจำ เพราะ '.$deposit_txt;
	}

	$sohd_values = array(
			'SOID' => $soid,
			'SaleAreaID' => $order['SaleAreaID'],
			'VATGroupID' => $order['VATGroupID'],
			'EmpID' => $order['EmpID'],
			'BrchID' => $order['BrchID'],
			'TranspID' => $order['TranspID'],
			'DocuNo' => $docu_no,
			'CustID' => $emcust['CustID'],
			'CustName' => $emcust['CustName'],
			'DocuType' => '104',
			'DocuStatus' => 'N',
			'DocuDate' => $order['DocuDate'].' '.$order['DocuTime'],
			'ExpireDate' => $order['DocuExpDate'].' '. '00:00:00',
			'OnHold' => 'N',
			'VatRate' => $order['VATRate'],
			'VatType' => $order['VatType'],
			'GoodType' => 1,
			'ContactName' => $order['CustContact'],
			'CreditDays' => $order['CreditDays'],
			'ExchRate' => 1,
			'ShipDate' => $order['ShippingDate'].' '.$order['ShippingTime'],
			'SumGoodAmnt' => $order['SumGoodAmnt'],
			'BaseDiscAmnt' => 0,
			'BillDiscFormula' => $order['BillDiscFormula'],
			'BillDiscAmnt' => 0,
			'BillAftrDiscAmnt' => $order['NetAmnt'],
			'TotaExcludeAmnt' => 0,
			'TotaBaseAmnt' => $order['TotaBaseAmnt'],
			'VATAmnt' => $vat,
			'NetAmnt' => $order['NetAmnt'],
			'ValidDays' => 0,
			'SumIncludeAmnt'=>0,
			'SumExcludeAmnt'=>0,
			'CommissionAmnt' =>0,
			'MiscChargAmnt' =>0,
			'MultiCurrency' =>'N',
			'ResvAmnt1'	=>0,
			'ResvAmnt2'	=>0,
			'ResvAmnt3'	=>0,
			'ResvAmnt4'	=>0,
			'QuotStatus' => 'รอผู้ใหญ่ตัดสินใจ',
			'AppvFlag' => 'W',
			'PkgStatus' => 'N',
			'is_new' => true,
			'JobID' => $jobid,
			'ClearSO' => 'N'
	);
	$sql = _arrayToSql('SOHD', $sohd_values);
	watchdog('kcrestws' , 'sohd-sql['.$sql.']');
	$sohd = entity_create('SOHD', $sohd_values);
	$sohd->save();

	$sohd_remark_values1 = array(
			'ListNo' => 1,
			'SOID' => $soid,
			'Remark' => $order['ShippingTime'],
	);
	$sohd_remark_values2 = array(
			'ListNo' => 2,
			'SOID' => $soid,
			'Remark' => $deposit_show,
	);
	watchdog('DB debug', 'sohd_remark_values1  '.var_export($sohd_remark_values1, true));
	$sohd = entity_create('SOHDRemark', $sohd_remark_values1);
	$sohd->save();
	watchdog('DB debug', 'sohd_remark_values2  '.var_export($sohd_remark_values2, true));
	$sohd = entity_create('SOHDRemark', $sohd_remark_values2);
	$sohd->save();
	return $order;
}

function _order_resource_create_sodt_roof_remark(&$order_line){
	$length = sprintf ("%.2f", $order_line['Length']);
	$remarks = array();
	$remarks[] = _isReverseColor($order_line['ReverseColor']);
	$remarks[] = $order_line['Kong'];
	$remarks[] = $order_line['Krob'];
	//$remarks[] = 'ยาว '.$order_line['Length'].' ม. x ';
	$remarks[] = 'ยาว '.$length.' ม. x ';
	$remarks[] = $order_line['Qty'].' แผ่น';
	//$remarks[] = $order_line['Remark_tablet'];

	unset($order_line['Qty'], $order_line['Length']);
	//$remark = _isReverseColor($order_line['ReverseColor']).$order_line['Kong'].$tmp[$order_line['GoodID']]['Krob'].' '.$tmp[$order_line['GoodID']]['Length'].' ม. X '.$tmp[$order_line['GoodID']]['Qty'].' แผ่น';
	return implode(' ', $remarks);
}

function _order_resource_create_sodt_mark($order_lines){
	$grouping_good = variable_get('grouping_good');
	$grouping_good_array = explode(",",$grouping_good) ;
	//$arr = array(1001,1003,1004);
	$new_sodts = array();
	foreach ($order_lines as $order_line){
		$goodGroupID = $order_line['GoodGroupID'];
		if (in_array($goodGroupID, $grouping_good_array)){
			$order_line['grouping']=0;
		}else {
			$order_line['grouping']=1;
		}
		$new_sodts []= $order_line;
	}
	watchdog('kcrestws', 'sodt Grouping MARK  '.var_export($new_sodts, true));
	return $new_sodts;
}
function _order_resource_create_sodt_grouping($order_lines){
	$new_sodts = array();
	$remarks = array();
	$i=0;
	foreach ($order_lines as $order_line){
		if ($order_line['grouping']==1){
			$good_id = $order_line['GoodID'];
		}elseif($order_line['grouping']>=10){
			$good_id = $order_line['GoodID'].'-'.$order_line['grouping'];

		}else {
			$good_id = $order_line['GoodID'].'_'.$i;
		}
		$isRoof = isset($order_line['Length'])?true:false;
		
		if (isset($count_sodt[$good_id])){
			$count_sodt[$good_id]++;
		}else {
			$count_sodt[$good_id]=0;
		}
		$good_id = _order_resource_set_index_by_goodid($good_id,$count_sodt[$good_id]);


		if($isRoof){
			$remark = _order_resource_create_sodt_roof_remark($order_line);
			$remarks[$good_id][$i] = $remark;
			if (isset($order_line['Remark_tablet']) && $order_line['Remark_tablet'] !=''){
				$remarks[$good_id][99] = $order_line['Remark_tablet'];
			}
		}


		if(isset($new_sodts[$good_id])){
			$sodt = &$new_sodts[$good_id];
			$sodt['TotalLength'] += $order_line['TotalLength'];
			$sodt['OLTotal'] += $order_line['OLTotal'];
			$sodt['Discount'] += $order_line['Discount'];
			$sodt['DiscountedTotal'] += $order_line['DiscountedTotal'];
		}else{
			$new_sodts[$good_id] = $order_line;
			$new_sodts[$good_id]['is_roof'] = $isRoof;
		}
		$i++;
	}

	foreach($new_sodts as $good_id => &$sodt){
		if(isset($remarks[$good_id])){
			ksort($remarks[$good_id]);
			$sodt['remarks'] = $remarks[$good_id];
		}else{
			$sodt['remarks'] = array();
		}
	}
	//watchdog('kcrestws', 'sodt grouping  '.var_export($new_sodts, true));
	//$new_sodts2 = _order_resource_create_sodt_regrouping($new_sodts);
	watchdog('kcrestws', 'sodt regrouping  '.var_export($new_sodts, true));


	return $new_sodts;
}
function _order_resource_set_index_by_goodid($good_id,$count_good_id){
	$c = ((INT)($count_good_id/10));
	$return = $good_id.'C'.$c;
	return $return;
}
function _order_resource_create_sodt_regrouping($new_sodts){
	$arr = array(1001,1003,1004);
	$new_sodts2=array();
	foreach ($new_sodts as $new_sodt){
		if (in_array($new_sodt['GoodGroupID'],$arr)){
			$remark=$new_sodt['remarks'];
			for($i=0;$i<count($remark);$i++){
				//	dsm($remark[$i]);
				$new_sodt['remarks'] = array($remark[$i]);
				//	$new_sodt['remarks'] = $remark[$i];
				$new_sodts2[]= $new_sodt;
			}
		}else {
			$new_sodts2[]= $new_sodt;
		}
	}
	return $new_sodts2;
}
function _order_resource_create_sodt_mark_Krob_Kong($order_lines){
	$mark_group=10;
	for ($i=0;$i<count($order_lines)-1;$i++){
		if (($order_lines[$i]['Krob']==$order_lines[$i+1]['Krob']) && isset($order_lines[$i]['Krob']) ){
			if ($order_lines[$i]['grouping']==0){
				$order_lines[$i+1]['grouping']=$order_lines[$i]['grouping']=$mark_group;
				$mark_group++;
			}else {
				$order_lines[$i+1]['grouping']=$order_lines[$i]['grouping'];
			}
		}elseif (($order_lines[$i]['Kong']==$order_lines[$i+1]['Kong']) && isset($order_lines[$i]['Kong']) ){
			if ($order_lines[$i]['grouping']==0){
				$order_lines[$i+1]['grouping']=$order_lines[$i]['grouping']=$mark_group;
				$mark_group++;
			}else {
				$order_lines[$i+1]['grouping']=$order_lines[$i]['grouping'];
			}
		}
	}
	return $order_lines;
}
function _order_resource_create_sodt($order, $order_lines, $jobid){
	$soid = $order['SOID'];
	$i=1;


	foreach ($order_lines as $order_line){
		$is_roof = $order_line['is_roof'];
		$goodID = $order_line['GoodID'];
		$Good = _getGoodID($goodID);
		$GoodUnit = $Good->MainGoodUnitID;
		$sodt_values = array(
				'SOID' 					=> $soid,
				'ListNo' 				=> $i,
				'DocuType' 				=> '104',
				'GoodID'				=> $goodID,
				'GoodName'				=> $order_line['GoodName1'],
				'InveID'				=> $order['InveID'],
				'LocaID'				=> $order['LocaID'],
				'GoodPrice1'			=> 0,
				'GoodQty1'				=> 0,
				'GoodUnitID2'			=> $GoodUnit,
				'GoodStockRate1'		=> 0,
				'GoodQty2'				=> $is_roof?$order_line['TotalLength']:$order_line['Qty'],
				'GoodPrice2'			=> $order_line['Price'],
				'GoodDiscAmnt'			=> $order_line['Discount'],
				'GoodDiscFormula'		=> $order_line['Discount'],
				'GoodStockRate2'		=> 1,
				'MiscChargAmnt' 		=> 1,
				'SumExcludeAmnt'		=> 0,
				'GoodCompareQty'		=> 0,
				'GoodAmnt'				=> $order_line['OLTotal'],
				'ShipDate' 				=> $order['ShippingDate'],
				'RemaBefoQty' 			=> 0,
				'LotFlag'				=> 'N',
				'GoodType'				=> 1,
				'SerialFlag' 			=> 'N',
				'GoodStockUnitID' 		=> $GoodUnit,
				'GoodStockQty' 			=> $is_roof?$order_line['TotalLength']:$order_line['Qty'],
				'GoodCost'				=> 0,
				'VatType'				=> 1,
				'StockFlag' 			=> -1,
				'GoodFlag' 				=> 'G',    //G,S EMood.GoodTypeFlag
				'RemaQty'				=> $is_roof?$order_line['TotalLength']:$order_line['Qty'],
				'ReserveQty' 			=> 0,
				'FreeFlag' 				=> 'N',
				'ResvAmnt1' 			=> 0,
				'ResvAmnt2' 			=> 0,
				'GoodRemaQty1' 			=> $is_roof?$order_line['TotalLength']:$order_line['Qty'],
				'GoodRemaQty2' 			=> 0,
				'MarkUpAmnt'			=> 0,
				'CommisAmnt' 			=> 0,
				'POQty'					=> $is_roof?$order_line['TotalLength']:$order_line['Qty'],
				'RemaQtyPkg'			=> $is_roof?$order_line['TotalLength']:$order_line['Qty'],
				'Expireflag'			=> 'N',
				'Poststock'				=> 'N',
				'AfterMarkupamnt'		=> $order_line['OLTotal'],
				'RemaGoodStockQty' 		=> 0,
				'remaamnt'				=> $order_line['DiscountedTotal'],
				'goodlong'				=> 0,
				'sheetqty'				=> 0,
				'JobID' 				=> $jobid
		);
		watchdog('DB debug', 'sodt_values  '.var_export($sodt_values, true));
		//	watchdog('kcrestws', '$cust: '$cust, true));
		$soid1 = entity_create('SODT', $sodt_values);
		$soid1->save();

		$remarks = $order_line['remarks'];
		$j=1;
		foreach ($remarks as $line){
			$sodt_remark = array(
					'ListNo' => $j,
					'SOID' 	=> $soid,
					'RefListNo' => $i,
					'Remark' => $line
			);
			$soid_re = entity_create('SODTRemark', $sodt_remark);
			$soid_re->save();
			
			$sql = _arrayToSql('SODTRemark', $sodt_remark);
			
			watchdog('kcrestws' , 'SODT REMARK sql: '.$sql);
			$j++;
		}
		/*
		for($j=0; $j<count($remarks); $j++){
			$sodt_remark = array(
					'ListNo' => $j+1,
					'SOID' 	=> $soid,
					'RefListNo' => $i,
					'Remark' => $remarks[$j]
			);
			$soid_re = entity_create('SODTRemark', $sodt_remark);
			$soid_re->save();

			$sql = _arrayToSql('SODTRemark', $sodt_remark);

			watchdog('kcrestws' , 'SODT REMARK sql: '.$sql);
			$jj=$j;
		}
		*/
		$i++;


	}
}
function  _order_resource_search_emcust($order){
	$CustCode = $order['CustCode'];
	$CustName = $order['CustName'];
	$query_EMCust = db_select("EMCust", 'o')
	->fields('o')
	->condition('CustCode', $CustCode)
	->condition('CustName',$CustName);
	$emcust = $query_EMCust->execute()->fetchAssoc();
	return $emcust;
}
function _order_resource_create_emcust($emcust){
	watchdog('kcrestws' , 'emcustdebug: '.$emcust);
	if ($emcust['IsNew']==true) {
		watchdog('kcrestws' , 'NEW  emcc: ');
		$ret = db_query('select max(CustID)+1 from EMCust')->fetchCol();
		$custid = $ret[0];
		watchdog('kcrestws' , 'custid '.$custid);
		$emcust_values = array(
				"CustID" 		=> $custid,
				"CustName" 		=> $emcust['CustName'],
				"CustGroupID"	=> $emcust['CustGroupID'],
				"Province"		=> $emcust['Province'],
				"Amphur" 		=> $emcust['Amphur'],
				"District"	 	=> $emcust['District'],
				"PostCode"		=> $emcust['PostCode'],
				"ContTel"		=> $emcust['ContTel'],
				"ContFax"		=> $emcust['ContFax'],
				"BrchID"		=> $emcust['BrchID'],
				"CustCode"		=> $emcust['CustCode'],
				"SaleAreaID"	=> $emcust['SaleAreaID'],
				"CreditAmnt"	=> 0.00,
				"ListType"		=> 'C',
				"Inactive"		=> 'A',
				"CustType"		=> 2,
				"CreditState"	=> 1,
				"EditGoodDisc"	=> 'N',
				"EditBillDisc"	=> 'N',
				"CustAddr1"		=> $emcust['CustAddr1'],
				"ContAddr1"		=> $emcust['CustAddr1'],
				"ContDistrict"	=> $emcust['District'],
				"ContAmphur" 		=> $emcust['Amphur'],
				"ContProvince"		=> $emcust['Province'],
				"ContPostCode"		=> $emcust['PostCode'],
				"ThaiOwner"		=>	'N',
				"VAttype"		=> $emcust['VatType'],
				"saletype"	=>1,
		);
		//				watchdog('kcrestws' , 'emcust_values: '.$emcust_values);
		$emc = entity_create('EMCust', $emcust_values);
		$emc->save();
		$sql = _arrayToSql('EMCust', $emcust_values);
		watchdog('kcrestws' , 'emcust-sql['.$sql.']');

		return $emcust_values;
	}
	else{

	}
}


function _order_resource_create($records) {
	watchdog("kcrestws", 'incoming object: '.var_export($records, true));
	$results = array();

	foreach($records as $record){

	//	$sohd = $order;

		try{
			watchdog('kcrestws', 'record: '.var_export($record, true));
			$data = $record['data'];
			////////////////////////////////////////////////////
			$cust = drupal_json_decode($data['EMCust']);
			$order = drupal_json_decode($data['Order']);
			$order_lines = drupal_json_decode($data['OrderLine']);
			watchdog('kcrestws', '$cust: '.var_export($cust, true));

			$jobid = 1000;
			//	$jobid = _order_resource_set_jobid($order_lines);
			//	watchdog('DB debug', '$jobid  '.var_export($jobid, true));

			//mark oderline
			$sodts_mark = _order_resource_create_sodt_mark($order_lines);
			//mark Krob Kong
			$sodts_mark = _order_resource_create_sodt_mark_Krob_Kong($sodts_mark);
			watchdog('kcrestws', 'sodt_mark_Krob_Kong  $sodts_mark: '.var_export($sodts_mark, true));
			//prepare data
			$sodts = _order_resource_create_sodt_grouping($sodts_mark);
			$cust['VatType']=$order['VatType'];


			//$jobid;
			//insert into database
			$emcust = _order_resource_create_emcust($cust);
			// fix bug New EMCust
			$emcust = _order_resource_search_emcust($order);
			$sohd = _order_resource_create_sohd($emcust, $order ,$jobid);
			$sodt = _order_resource_create_sodt($sohd, $sodts ,$jobid);




			$result = array();
			$result['Success'] = true;
			$result['ID'] = $record['id'];
			$result['SOID'] = $sohd['SOID'];
			$result['DocuNo'] = $sohd['DocuNo'];
			$result['OrderStatus'] = "เรียบร้อย";
			$result['data'] = $sohd;

			$results[] = $result;

		}catch(Exception $e){
			$result['Success'] = false;
			$result['ID'] = $record['id'];
			$result['SOID'] = $sohd['SOID'];
			$result['DocuNo'] = $sohd['DocuNo'];
			$result['OrderStatus'] = 'รอส่ง';
			$results[] = $result;
			watchdog('error', $e->getMessage());
		}

	}

	return base64_encode(gzcompress(serialize($results)));
}
function _isReverseColor($input){
	if ($input)
		return 'กลับลอนรีด ';
	else
		return '';
}
function _getGoodID($id){
	return entity_load_single('EMGood', $id);
}
function _order_resource_set_SaleAreaID($CustGroupID){
	$CustGroupCode = _order_resource_search_CustGroupCode($CustGroupID);
	$SaleAreaID = _order_resource_search_SaleAreaID($CustGroupCode);
	if (!(isset($SaleAreaID))) {
		watchdog('kcrestws' , '$SaleAreaID not set ');
		$SaleAreaID =0;
	}
	return $SaleAreaID;
}
function _order_resource_search_CustGroupCode($CustGroupID){
	$query = db_select("EMCustGroup", 'o')
	->fields('o')
	->condition('CustGroupID', $CustGroupID);
	$EMCustGroup = $query->execute()->fetchAssoc();
	return $EMCustGroup['CustGroupCode'];
}
function _order_resource_search_SaleAreaID($CustGroupCode){
	$query = db_select("EMSaleArea", 'o')
	->fields('o')
	->condition('SaleAreaCode', $CustGroupCode);
	$EMSaleArea = $query->execute()->fetchAssoc();
	return $EMSaleArea['SaleAreaID'];
}
function _order_resource_set_jobid($order_lines){
	watchdog('DB debug', 'set_jobid  '.var_export(variable_get('jobid_good'), true).var_export($order_lines, true));
	$jobid_good = variable_get('jobid_good');
	$jobid_good_array = explode(",",$jobid_good) ;
	foreach ($order_lines as $order_line){
		$goodGroupID = $order_line['GoodGroupID'];
		if (in_array($goodGroupID, $jobid_good_array)){
			return 1000;
		}
	}
	return NULL;
}
function _order_resource_create_jcjob($emcust ,$order, $order_lines, $jobid){
	$docu_no = _kc_restws_getNextDocuNo_Job($order['DocuNo']);
	$soid = $order['SOID'];
	$i=0;

	foreach ($order_lines as $order_line){
		$ret = db_query('select max(JCID)+1 from JCOrder')->fetchCol();
		$jcid = $ret[0];
		if ($i>0){
			$docu_no_input = $docu_no.'/'.$i;
		}else{
			$docu_no_input = $docu_no;
		}
		$is_roof = $order_line['is_roof'];
		$goodID = $order_line['GoodID'];
		$Good = _getGoodID($goodID);
		$GoodUnit = $Good->MainGoodUnitID;
		$jod_values = array(
				'JCID' => $jcid,
				'JobID' => $jobid,
				'BrchID'	=> $order['BrchID'],
				'DocuDate'	=> $order['DocuDate'].' '.$order['DocuTime'],
				'Docuno'	=> $docu_no_input,
				'GoodID'	=> $order_line['GoodID'],
				'RemaQty'	=> 0,
				'RecpQty'	=> 0,
				//'DeptID'	=> '???',
				'GoodQty'	=> $is_roof?$order_line['TotalLength']:$order_line['Qty'],
				'ShipDays' 	=> 1,
				'ShipDate'	=> $order['DocuDate'].' '.'00:00:00',
				'StartDate' => $order['DocuDate'].' '.'00:00:00',
				'Docustatus'=> 'N',
				'GoodAmnt'	=> $order_line['OLTotal'],
				'ExprFlag'	=> 'N',
				'LotFlag'	=> 'N',
				'PackQty'	=> 0,
				'PackRate'	=> 0,
				'Docutype'	=> '1301',
				'EmpID'		=> $order['EmpID'],
				'GoodUnitID'=> $GoodUnit,
				'PreFlag'	=> 'N',
				'OnHold'	=> 'N',
				'CanCelFlag'=> 'N',
				//'JobFlag'	=> 1,
				'SOID'		=> $order['SOID'],
				'CustID'	=> $emcust['CustID'],
				'GoodName'	=> $order_line['GoodName1'],
				'SONO'		=> $order['DocuNo'],
				'GainBaseAmnt' => 0,
				'GainTotaAmnt' => 0,
				'SumGoodAmnt' => $is_roof?$order_line['TotalLength']:$order_line['Qty'],
				'JobFlag'	=>1,
				'JCTypeOH'	=>'N',
				'JCTypeDL'	=>'N',
				'QCFlag'	=>0
		);
		watchdog('DB debug', 'JCOrder  '.var_export($jod_values, true));
		//	watchdog('kcrestws', '$cust: '$cust, true));
		//$soid1 = entity_create('JCOrder', $jod_values);
		//$soid1->save();
		$nid = db_insert('JCOrder') // Table name no longer needs {}
		->fields($jod_values
		)
		->execute();
		watchdog('DB debug', 'JCOrder  nid'.var_export($nid, true));
		$i++;
	}
}
