<?php

class EMGoodEntity extends Entity {
    
}

class EMGoodTypeEntity extends Entity {
    
}
 
class EMGoodGroupEntity extends Entity {
    
}

class EMGoodCateEntity extends Entity {
    
}

class EMGoodBrandEntity extends Entity {
    
}

class EMGoodColorEntity extends Entity {
    
}

class EMCustEntity extends Entity {
    
}

class EMCustContactEntity extends Entity {
    
}

class EMCustGroupEntity extends Entity {
    
}

class EMCustMultiAccEntity extends Entity {
    
}

class EMCustMultiEmpEntity extends Entity {
    
}

class EMCustPicEntity extends Entity {
    
}

class EMCustTypeEntity extends Entity {
    
}

class EMEmpEntity extends Entity {
    
}

class EMEmpGroupEntity extends Entity {
    
}

class EMTranspEntity extends Entity {
    
}

class EMSalesEntity extends Entity {
    
}

class EMSaleAreaEntity extends Entity {
    
}

class SOHDEntity extends Entity {
    
}

class SODTRemarkEntity extends Entity {
    
}

class SODTEntity extends Entity {
    
}

class SOHDRemarkEntity extends Entity {
    
}

class EMVATGroupEntity extends Entity {
    
}

class EMBrchEntity extends Entity {
    
}

class EMDeptEntity extends Entity {
    
}

class EMVendorEntity extends Entity {
    
}

class EMVendorContactEntity extends Entity {
    
}

class EMReceivePlaceEntity extends Entity {
    
}

class EMGoodUnitEntity extends Entity {
    
}

class EMInveEntity extends Entity {

    function getBranch() {
        return reset(entity_load('EMBrch', FALSE, array('BrchCode' => $this->InveCode)));
    }

}

class EMGoodOrderEntity extends Entity {
    
}

class ICStockDetailEntity extends Entity {
    
}
class POHDEntity extends Entity{}
class PODTEntity extends Entity{}
class VwPrAppvFlagEntity extends Entity{}
class EMPostEntity extends Entity{}

class EMGoodViewsController extends EntityDefaultViewsController {

    protected function schema_fields() {
        $data = parent::schema_fields();
        $data['GoodGroupID']['relationship'] = array(
            'label' => t('Good Group'),
            'base' => 'EMGoodGroup',
            'field' => 'GoodGroupID',
            'handler' => 'views_handler_relationship',
        );
        $data['GoodTypeID']['relationship'] = array(
            'label' => t('Good Type'),
            'base' => 'EMGoodType',
            'field' => 'GoodTypeID',
            'handler' => 'views_handler_relationship',
        );
        $data['GoodBrandID']['relationship'] = array(
            'label' => t('Good Brand'),
            'base' => 'EMGoodBrand',
            'field' => 'GoodBrandID',
            'handler' => 'views_handler_relationship',
        );
        $data['GoodCateID']['relationship'] = array(
            'label' => t('Good Cate'),
            'base' => 'EMGoodCate',
            'field' => 'GoodCateID',
            'handler' => 'views_handler_relationship',
        );
        $data['GoodColorID']['relationship'] = array(
            'label' => t('Good Color'),
            'base' => 'EMGoodColor',
            'field' => 'GoodColorID',
            'handler' => 'views_handler_relationship',
        );
        $data['MainGoodUnitID']['relationship'] = array(
            'label' => t('MainGoodUnitID'),
            'base' => 'EMGoodUnit',
            'field' => 'GoodUnitID',
            'handler' => 'views_handler_relationship',
            'relationship field' => 'MainGoodUnitID',
        );
        $data['GoodID']['relationship'] = array(
            'label' => t('ICStockDetail EMGOOD'),
            'base' => 'ICStockDetail',
            'field' => 'GoodID',
            'handler' => 'views_handler_relationship',
        );
        return $data;
    }

}

class EMGoodTypeViewsController extends EntityDefaultViewsController {

    protected function schema_fields() {
        $data = parent::schema_fields();
        return $data;
    }

}

class EMGoodGroupViewsController extends EntityDefaultViewsController {

    protected function schema_fields() {
        $data = parent::schema_fields();
        return $data;
    }

}

class EMGoodCateViewsController extends EntityDefaultViewsController {

    protected function schema_fields() {
        $data = parent::schema_fields();
        return $data;
    }

}

class EMGoodBrandViewsController extends EntityDefaultViewsController {

    protected function schema_fields() {
        $data = parent::schema_fields();
        return $data;
    }

}

class EMGoodColorViewsController extends EntityDefaultViewsController {

    protected function schema_fields() {
        $data = parent::schema_fields();
        return $data;
    }

}

class EMEmpViewsController extends EntityDefaultViewsController {

    protected function schema_fields() {
        $data = parent::schema_fields();
        $data['EmpGroupID']['relationship'] = array(
            'label' => t('Emp Group'),
            'base' => 'EMEmpGroup',
            'field' => 'EmpGroupID',
            'handler' => 'views_handler_relationship',
        );
        $data['DeptID']['relationship'] = array(
            'label' => t('EMDept'),
            'base' => 'EMDept',
            'field' => 'DeptID',
            'handler' => 'views_handler_relationship',
        );
        $data['PostID']['relationship'] = array(
        		'label' => t('EMPost'),
        		'base' => 'EMPost',
        		'field' => 'PostID',
        		'handler' => 'views_handler_relationship',
        );
        return $data;
    }

}

class EMEmpGroupViewsController extends EntityDefaultViewsController {

    protected function schema_fields() {
        $data = parent::schema_fields();
        return $data;
    }

}

class EMCustViewsController extends EntityDefaultViewsController {

    protected function schema_fields() {
        $data = parent::schema_fields();
        $data['CustGroupID']['relationship'] = array(
            'label' => t('Cust Group'),
            'base' => 'EMCustGroup',
            'field' => 'CustGroupID',
            'handler' => 'views_handler_relationship',
        );
        return $data;
    }

}

class EMCustContactViewsController extends EntityDefaultViewsController {

    protected function schema_fields() {
        $data = parent::schema_fields();
        $data['CustID']['relationship'] = array(
            'label' => t('Cust Contact'),
            'base' => 'EMCust',
            'field' => 'CustID',
            'handler' => 'views_handler_relationship',
        );
        return $data;
    }

}

class EMCustGroupViewsController extends EntityDefaultViewsController {

    protected function schema_fields() {
        $data = parent::schema_fields();
        return $data;
    }

}

class EMCustMultiAccViewsController extends EntityDefaultViewsController {

    protected function schema_fields() {
        $data = parent::schema_fields();
        return $data;
    }

}

class EMCustMultiEmpViewsController extends EntityDefaultViewsController {

    protected function schema_fields() {
        $data = parent::schema_fields();
        return $data;
    }

}

class EMCustPicViewsController extends EntityDefaultViewsController {

    protected function schema_fields() {
        $data = parent::schema_fields();
        return $data;
    }

}

class EMCustTypeViewsController extends EntityDefaultViewsController {

    protected function schema_fields() {
        $data = parent::schema_fields();
        return $data;
    }

}

class EMTranspViewsController extends EntityDefaultViewsController {

    protected function schema_fields() {
        $data = parent::schema_fields();
        return $data;
    }

}

class EMSaleAreaViewsController extends EntityDefaultViewsController {

    protected function schema_fields() {
        $data = parent::schema_fields();
        return $data;
    }

}

class SOHDViewsController extends EntityDefaultViewsController {

    protected function schema_fields() {
        $data = parent::schema_fields();
        $data['SOID']['relationship'] = array(
            'label' => t('SOHD - SODT'),
            'base' => 'SODT',
            'field' => 'SOID',
            'handler' => 'views_handler_relationship',
        );
        $data['SOID']['relationship'] = array(
            'label' => t('SOHD - SOHDRemark'),
            'base' => 'SOHDRemark',
            'field' => 'SOID',
            'handler' => 'views_handler_relationship',
        );
        $data['DocuDate'] = array(
            'title' => t('DocuDate'), // The item it appears as on the UI,
            'help' => t('DocuDate'),
            'field' => array(
                'handler' => 'views_handler_field_date',
                'click sortable' => TRUE,
            ),
            'sort' => array(
                'handler' => 'views_handler_sort_date',
            ),
            'filter' => array(
                'handler' => 'views_handler_filter_date',
            ),
        );
        $data['CustID']['relationship'] = array(
            'label' => t('EMCust CustID'),
            'base' => 'EMCust',
            'field' => 'CustID',
            'handler' => 'views_handler_relationship',
        );
        return $data;
    }

}

class SODTViewsController extends EntityDefaultViewsController {

    protected function schema_fields() {
        $data = parent::schema_fields();
        $data['SOID']['relationship'] = array(
            'label' => t('SOHDRemark SOID'),
            'base' => 'SOHDRemark',
            'field' => 'SOID',
            'handler' => 'views_handler_relationship',
        );
        return $data;
    }

}

class SODTRemarkViewsController extends EntityDefaultViewsController {

    protected function schema_fields() {
        $data = parent::schema_fields();
        return $data;
    }

}

class SOHDRemarkViewsController extends EntityDefaultViewsController {

    protected function schema_fields() {
        $data = parent::schema_fields();
        return $data;
    }

}

class EMVATGroupViewsController extends EntityDefaultViewsController {

    protected function schema_fields() {
        $data = parent::schema_fields();
        return $data;
    }

}

class EMSalesViewsController extends EntityDefaultViewsController {

    protected function schema_fields() {
        $data = parent::schema_fields();
        $data['EmpID']['relationship'] = array(
            'label' => t('Cust Contact'),
            'base' => 'EMEmp',
            'field' => 'EmpID',
            'handler' => 'views_handler_relationship',
        );
        return $data;
    }

}

class EMBrchViewsController extends EntityDefaultViewsController {

    protected function schema_fields() {
        $data = parent::schema_fields();
        return $data;
    }

}

class EMDeptViewsController extends EntityDefaultViewsController {

    protected function schema_fields() {
        $data = parent::schema_fields();
        return $data;
    }

}

class EMVendorViewsController extends EntityDefaultViewsController {

    public function views_data() {
        $data = parent::views_data();
        if (!empty($this->info['base table'])) {
            $table = $this->info['base table'];
            $data[$table]['table']['base']['database'] = 'default';
        }
        return $data;
    }

    protected function schema_fields() {
        $data = parent::schema_fields();
        $data['VendorID']['relationship'] = array(
            'label' => t('Vendor ID'),
            'base' => 'EMVendorContact',
            'field' => 'VendorID',
            'handler' => 'views_handler_relationship',
        );
        return $data;
    }

}

class EMVendorContactViewsController extends EntityDefaultViewsController {

    public function views_data() {
        $data = parent::views_data();
        if (!empty($this->info['base table'])) {
            $table = $this->info['base table'];
            $data[$table]['table']['base']['database'] = 'default';
        }
        return $data;
    }

    protected function schema_fields() {
        $data = parent::schema_fields();
        return $data;
    }

}

class EMReceivePlaceViewsController extends EntityDefaultViewsController {

    public function views_data() {
        $data = parent::views_data();
        if (!empty($this->info['base table'])) {
            $table = $this->info['base table'];
            $data[$table]['table']['base']['database'] = 'default';
        }
        return $data;
    }

    protected function schema_fields() {
        $data = parent::schema_fields();
        return $data;
    }

}

class EMGoodUnitViewsController extends EntityDefaultViewsController {

    public function views_data() {
        $data = parent::views_data();
        if (!empty($this->info['base table'])) {
            $table = $this->info['base table'];
            $data[$table]['table']['base']['database'] = 'default';
        }
        return $data;
    }

    protected function schema_fields() {
        $data = parent::schema_fields();
        return $data;
    }

}

class EMInveViewsController extends EntityDefaultViewsController {

    public function views_data() {
        $data = parent::views_data();
        if (!empty($this->info['base table'])) {
            $table = $this->info['base table'];
            $data[$table]['table']['base']['database'] = 'default';
        }
        return $data;
    }

    protected function schema_fields() {
        $data = parent::schema_fields();
                $data = parent::schema_fields();
        $data['BrchID']['relationship'] = array(
            'label' => t('EM Brch'),
            'base' => 'EMBrch',
            'field' => 'BrchID',
            'handler' => 'views_handler_relationship',
        );
        return $data;
    }

}

class EMGoodOrderViewsController extends EntityDefaultViewsController {

    public function views_data() {
        $data = parent::views_data();
        if (!empty($this->info['base table'])) {
            $table = $this->info['base table'];
            $data[$table]['table']['base']['database'] = 'default';
        }
        return $data;
    }

    protected function schema_fields() {
        $data = parent::schema_fields();
        return $data;
    }

}

class ICStockDetailViewsController extends EntityDefaultViewsController {

    public function views_data() {
        $data = parent::views_data();
        if (!empty($this->info['base table'])) {
            $table = $this->info['base table'];
            $data[$table]['table']['base']['database'] = 'default';
        }
        return $data;
    }

    protected function schema_fields() {
        $data = parent::schema_fields();
        $data['GoodID']['relationship'] = array(
            'label' => t('EM Good'),
            'base' => 'EmGood',
            'field' => 'GoodID',
            'handler' => 'views_handler_relationship',
        );
        $data['BrchID']['relationship'] = array(
            'label' => t('EM Brch'),
            'base' => 'EMBrch',
            'field' => 'BrchID',
            'handler' => 'views_handler_relationship',
        );

        return $data;
    }

}
class POHDViewsController extends EntityDefaultViewsController{
	public function views_data() {
		$data = parent::views_data();
		if (!empty($this->info['base table'])) {
				$table = $this->info['base table'];
				$data[$table]['table']['base']['database'] = 'default';
		}
		return $data;
	}

	protected function schema_fields() {
		$data = parent::schema_fields();
		$data['VendorID']['relationship'] = array(
				'label' => t('EMVendor'),
				'base' => 'EMVendor',
				'field' => 'VendorID',
				'handler' => 'views_handler_relationship',
		);
		$data['BrchID']['relationship'] = array(
				'label' => t('EMBrch'),
				'base' => 'EMBrch',
				'field' => 'BrchID',
				'handler' => 'views_handler_relationship',
		);
		return $data;
	}
}
class PODTViewsController extends EntityDefaultViewsController{
	public function views_data() {
		$data = parent::views_data();
		if (!empty($this->info['base table'])) {
				$table = $this->info['base table'];
				$data[$table]['table']['base']['database'] = 'default';
		}
		return $data;
	}

	protected function schema_fields() {
		$data = parent::schema_fields();
		$data['GoodID']['relationship'] = array(
				'label' => t('EMGood'),
				'base' => 'EMGood',
				'field' => 'GoodID',
				'handler' => 'views_handler_relationship',
		);
		return $data;
	}
}
class VwPrAppvFlagViewsController extends EntityDefaultViewsController{
	public function views_data() {
		$data = parent::views_data();
		if (!empty($this->info['base table'])) {
			$table = $this->info['base table'];
			$data[$table]['table']['base']['database'] = 'default';
		}
		return $data;
	}

	protected function schema_fields() {
		$data = parent::schema_fields();
		return $data;
	}
}
class EMPostViewsController extends EntityDefaultViewsController{
	public function views_data() {
		$data = parent::views_data();
		if (!empty($this->info['base table'])) {
			$table = $this->info['base table'];
			$data[$table]['table']['base']['database'] = 'default';
		}
		return $data;
	}

	protected function schema_fields() {
		$data = parent::schema_fields();
		
		return $data;
	}
}

