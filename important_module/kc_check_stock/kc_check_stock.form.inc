<?php
function kc_check_stock_form_view_stock($form,&$form_state,$brchID,$goodGroupID){
	//	dsm($brchID);
	//	dsm($goodGroupID);
	$array_goodGroup = explode(",", $goodGroupID);
	//	dsm($array_goodGroup);
	dsm($form_state);
	$GoodCode = isset($form_state['values']['GoodCode'])?$form_state['values']['GoodCode']:NULL;
	$GoodName1 = isset($form_state['values']['GoodName1'])?$form_state['values']['GoodName1']:NULL;
	$options = isset($form_state['values']['option'])?$form_state['values']['option']:0;
	$data = kc_check_stock_selete_vw_chkStock($brchID,$array_goodGroup,$options,$GoodCode,$GoodName1);
	dsm($data);
	$form['table_start'] = array(
			'#markup' => '<table>
			<tr>',
	);
	$form['GoodCode'] = array(
			'#type' => 'textfield',
			'#title' => t('รหัสสินค้า'),
			//'#default_value' => $node->title,
			//'#required' => TRUE,
			'#prefix' => '<td>',
			'#suffix' => '</td>',
			'#size' =>20,
	);
	$form['GoodName1'] = array(
			'#type' => 'textfield',
			'#title' => t('ชื่อสินค้า'),
			//'#default_value' => $node->title,
			//'#required' => TRUE,
			'#prefix' => '<td>',
			'#suffix' => '</td>',
			'#size' =>20,
	);
	$form['option'] = array(
			'#type' => 'select',
			'#title' => t('Selected'),
			'#options' => array(
					0 => t('ทั้งหมด'),
					1 => t('ต้ำกว่า Minpoint'),
			),
			'#prefix' => '<td>',
			'#suffix' => '</td>',
			
	);

	$form['search'] = array(
			'#type' => 'submit',
			'#value' => t('search'),
			'#submit' => array('kc_check_stock_form_view_stock_search'),
			'#prefix' => '<td>',
			'#suffix' => '</td>',
	);
	$form['table_end'] = array(
			'#markup' => '</tr></table>');
	$header = array(
			'GoodCode'=>'รหัสสินค้า',
			'GoodName1'=>'ชื่อสินค้า',
			//			'' =>'สินค้าคงเหลือ'
			"MinPoint" => 'ค่าต่ำสุด',
			"MaxPoint" => 'ค่าสูงสุด',
			"remaqty" => 'สินค้าคงเหลือ',
			"diff" => 'ผลต่างค่าต่ำสุด',
			"diff2" => "จำนวนความต้องการ", 
	);
	foreach ($data as $line){
		$key = $line->ListNo.'_'.$line->GoodID;
		$rows[$key] = array(
				'GoodCode' => check_plain($line->GoodCode),
				'GoodName1'=> check_plain($line->GoodName1),
				"MinPoint" => check_plain($line->MinPoint),
				"MaxPoint" => check_plain($line->MaxPoint),
				"remaqty" => check_plain($line->remaqty),
				"diff" => check_plain($line->diff),
				"diff2" => check_plain($line->diff2),
		);
		//	dsm($line);
		//		if ($i==5) break;
	}
	//รหัสสินค้า	 ชื่อสินค้า	 สินค้าคงเหลือ	 ค่าต่ำสุด	 ผลต่างค่าต่ำสุด	 ค่าสูงสุด
	$form['table'] = array(
			'#type' => 'tableselect',
			'#header' => $header,
			'#options' => $rows,
	);
	$form['submit'] = array(
			'#type' => 'submit',
			'#value' => t('Submit'),
	);
	return $form;
}
function kc_check_stock_form_view_stock_search($form,&$form_state){
	$form_state['rebuild'] = TRUE;
	//	dsm($form_state);
}
function kc_check_stock_form_view_stock_submit($form,&$form_state){
	$datas = array();
	$form_state['rebuild'] = TRUE;
	$table = $form_state['values']['table'];
	foreach ($table as $line){
		dsm($line);
		if ($line){
			//if ($line=)
			$array1 = explode("_", $line);
			$data['ListNo'] = $array1[0];
			$data['GoodID'] = $array1[1];
			$datas[]=$data;
		}
	}
	$json = json_encode($datas);
	drupal_goto("kc_cart/accept_json/" . $json);
	dsm($datas);
	dsm($form_state);
}
?>