$view = new view();
$view->name = 'view_raw_material';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'EMGood';
$view->human_name = 'View Raw material';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = ' �������ѵ�شԺ';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '20';
$handler->display->display_options['style_plugin'] = 'table';
/* Relationship: EMGood: MainGoodUnitID */
$handler->display->display_options['relationships']['MainGoodUnitID']['id'] = 'MainGoodUnitID';
$handler->display->display_options['relationships']['MainGoodUnitID']['table'] = 'EMGood';
$handler->display->display_options['relationships']['MainGoodUnitID']['field'] = 'MainGoodUnitID';
/* Field: EMGood: GoodCode */
$handler->display->display_options['fields']['GoodCode']['id'] = 'GoodCode';
$handler->display->display_options['fields']['GoodCode']['table'] = 'EMGood';
$handler->display->display_options['fields']['GoodCode']['field'] = 'GoodCode';
/* Field: EMGood: Emgood ID */
$handler->display->display_options['fields']['GoodID']['id'] = 'GoodID';
$handler->display->display_options['fields']['GoodID']['table'] = 'EMGood';
$handler->display->display_options['fields']['GoodID']['field'] = 'GoodID';
$handler->display->display_options['fields']['GoodID']['separator'] = '';
/* Field: EMGood: GoodName1 */
$handler->display->display_options['fields']['GoodName1']['id'] = 'GoodName1';
$handler->display->display_options['fields']['GoodName1']['table'] = 'EMGood';
$handler->display->display_options['fields']['GoodName1']['field'] = 'GoodName1';
/* Field: EMGood: BackReceQty */
$handler->display->display_options['fields']['BackReceQty']['id'] = 'BackReceQty';
$handler->display->display_options['fields']['BackReceQty']['table'] = 'EMGood';
$handler->display->display_options['fields']['BackReceQty']['field'] = 'BackReceQty';
$handler->display->display_options['fields']['BackReceQty']['set_precision'] = TRUE;
$handler->display->display_options['fields']['BackReceQty']['precision'] = '2';
$handler->display->display_options['fields']['BackReceQty']['separator'] = '';
/* Field: EMGood: MainGoodUnitID */
$handler->display->display_options['fields']['MainGoodUnitID']['id'] = 'MainGoodUnitID';
$handler->display->display_options['fields']['MainGoodUnitID']['table'] = 'EMGood';
$handler->display->display_options['fields']['MainGoodUnitID']['field'] = 'MainGoodUnitID';
$handler->display->display_options['fields']['MainGoodUnitID']['separator'] = '';
/* Field: EMGood: StockQty */
$handler->display->display_options['fields']['StockQty']['id'] = 'StockQty';
$handler->display->display_options['fields']['StockQty']['table'] = 'EMGood';
$handler->display->display_options['fields']['StockQty']['field'] = 'StockQty';
$handler->display->display_options['fields']['StockQty']['set_precision'] = TRUE;
$handler->display->display_options['fields']['StockQty']['precision'] = '2';
$handler->display->display_options['fields']['StockQty']['separator'] = '';
/* Field: EMGoodUnit: GoodUnitName */
$handler->display->display_options['fields']['GoodUnitName']['id'] = 'GoodUnitName';
$handler->display->display_options['fields']['GoodUnitName']['table'] = 'EMGoodUnit';
$handler->display->display_options['fields']['GoodUnitName']['field'] = 'GoodUnitName';
$handler->display->display_options['fields']['GoodUnitName']['relationship'] = 'MainGoodUnitID';
/* Filter criterion: EMGood: GoodCode */
$handler->display->display_options['filters']['GoodCode']['id'] = 'GoodCode';
$handler->display->display_options['filters']['GoodCode']['table'] = 'EMGood';
$handler->display->display_options['filters']['GoodCode']['field'] = 'GoodCode';
$handler->display->display_options['filters']['GoodCode']['operator'] = 'contains';
$handler->display->display_options['filters']['GoodCode']['exposed'] = TRUE;
$handler->display->display_options['filters']['GoodCode']['expose']['operator_id'] = 'GoodCode_op';
$handler->display->display_options['filters']['GoodCode']['expose']['label'] = 'GoodCode';
$handler->display->display_options['filters']['GoodCode']['expose']['operator'] = 'GoodCode_op';
$handler->display->display_options['filters']['GoodCode']['expose']['identifier'] = 'GoodCode';
$handler->display->display_options['filters']['GoodCode']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
);
/* Filter criterion: EMGood: GoodName1 */
$handler->display->display_options['filters']['GoodName1']['id'] = 'GoodName1';
$handler->display->display_options['filters']['GoodName1']['table'] = 'EMGood';
$handler->display->display_options['filters']['GoodName1']['field'] = 'GoodName1';
$handler->display->display_options['filters']['GoodName1']['operator'] = 'contains';
$handler->display->display_options['filters']['GoodName1']['exposed'] = TRUE;
$handler->display->display_options['filters']['GoodName1']['expose']['operator_id'] = 'GoodName1_op';
$handler->display->display_options['filters']['GoodName1']['expose']['label'] = 'GoodName1';
$handler->display->display_options['filters']['GoodName1']['expose']['operator'] = 'GoodName1_op';
$handler->display->display_options['filters']['GoodName1']['expose']['identifier'] = 'GoodName1';
$handler->display->display_options['filters']['GoodName1']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
);
/* Filter criterion: EMGoodUnit: GoodUnitName */
$handler->display->display_options['filters']['GoodUnitName']['id'] = 'GoodUnitName';
$handler->display->display_options['filters']['GoodUnitName']['table'] = 'EMGoodUnit';
$handler->display->display_options['filters']['GoodUnitName']['field'] = 'GoodUnitName';
$handler->display->display_options['filters']['GoodUnitName']['relationship'] = 'MainGoodUnitID';
$handler->display->display_options['filters']['GoodUnitName']['operator'] = 'contains';
$handler->display->display_options['filters']['GoodUnitName']['exposed'] = TRUE;
$handler->display->display_options['filters']['GoodUnitName']['expose']['operator_id'] = 'GoodUnitName_op';
$handler->display->display_options['filters']['GoodUnitName']['expose']['label'] = 'GoodUnitName';
$handler->display->display_options['filters']['GoodUnitName']['expose']['operator'] = 'GoodUnitName_op';
$handler->display->display_options['filters']['GoodUnitName']['expose']['identifier'] = 'GoodUnitName';
$handler->display->display_options['filters']['GoodUnitName']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
$handler->display->display_options['path'] = 'view-raw-material';
$handler->display->display_options['menu']['type'] = 'normal';
$handler->display->display_options['menu']['title'] = '�������ѵ�شԺ';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['name'] = 'main-menu';
$handler->display->display_options['menu']['context'] = 0;
