$view = new view();
$view->name = 'test';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'vw_chkStock';
$view->human_name = 'View Check Stock ';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = '.:: �礤�ѧ�Թ��� ::.';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '20';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['pager']['options']['quantity'] = '9';
$handler->display->display_options['style_plugin'] = 'table_highlighter';
$handler->display->display_options['style_options']['grouping'] = array(
  0 => array(
    'field' => 'GoodGroupName',
    'rendered' => 1,
    'rendered_strip' => 0,
  ),
);
$handler->display->display_options['style_options']['columns'] = array(
  'GoodCode' => 'GoodCode',
  'GoodName1' => 'GoodName1',
  'remaamnt' => 'remaamnt',
  'MinPoint' => 'MinPoint',
  'diff' => 'diff',
  'GoodUnitName' => 'GoodUnitName',
  'BrchID' => 'BrchID',
  'GoodGroupID' => 'GoodGroupID',
  'GoodGroupName' => 'GoodGroupName',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'GoodCode' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'GoodName1' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'remaamnt' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'MinPoint' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'diff' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'GoodUnitName' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'BrchID' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'GoodGroupID' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'GoodGroupName' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
/* Header: Global: PHP */
$handler->display->display_options['header']['php']['id'] = 'php';
$handler->display->display_options['header']['php']['table'] = 'views';
$handler->display->display_options['header']['php']['field'] = 'php';
$handler->display->display_options['header']['php']['php_output'] = '<?php
$BrchID =  $view->args[0];
echo "<a href=\'/check-stock2/$BrchID\'><h1 style=\'color:red\'> >> ���੾���Թ��ҷ�����Թ��ҵ�ӡ��Ҥ�� Minpoint << </h4></a>";

$param = $view->args[0]."/".$view->args[1];
echo "<a href=\'export/check-stock/$param\'>Export Report</a>";

?>
';
/* Field: Vw ChkStock: GoodCode */
$handler->display->display_options['fields']['GoodCode']['id'] = 'GoodCode';
$handler->display->display_options['fields']['GoodCode']['table'] = 'vw_chkStock';
$handler->display->display_options['fields']['GoodCode']['field'] = 'GoodCode';
/* Field: Vw ChkStock: GoodName1 */
$handler->display->display_options['fields']['GoodName1']['id'] = 'GoodName1';
$handler->display->display_options['fields']['GoodName1']['table'] = 'vw_chkStock';
$handler->display->display_options['fields']['GoodName1']['field'] = 'GoodName1';
/* Field: Vw ChkStock: Remaamnt */
$handler->display->display_options['fields']['remaamnt']['id'] = 'remaamnt';
$handler->display->display_options['fields']['remaamnt']['table'] = 'vw_chkStock';
$handler->display->display_options['fields']['remaamnt']['field'] = 'remaamnt';
$handler->display->display_options['fields']['remaamnt']['set_precision'] = TRUE;
$handler->display->display_options['fields']['remaamnt']['precision'] = '2';
/* Field: Vw ChkStock: MinPoint */
$handler->display->display_options['fields']['MinPoint']['id'] = 'MinPoint';
$handler->display->display_options['fields']['MinPoint']['table'] = 'vw_chkStock';
$handler->display->display_options['fields']['MinPoint']['field'] = 'MinPoint';
$handler->display->display_options['fields']['MinPoint']['precision'] = '0';
/* Field: Vw ChkStock: Diff */
$handler->display->display_options['fields']['diff']['id'] = 'diff';
$handler->display->display_options['fields']['diff']['table'] = 'vw_chkStock';
$handler->display->display_options['fields']['diff']['field'] = 'diff';
$handler->display->display_options['fields']['diff']['set_precision'] = TRUE;
$handler->display->display_options['fields']['diff']['precision'] = '2';
/* Field: Vw ChkStock: GoodUnitName */
$handler->display->display_options['fields']['GoodUnitName']['id'] = 'GoodUnitName';
$handler->display->display_options['fields']['GoodUnitName']['table'] = 'vw_chkStock';
$handler->display->display_options['fields']['GoodUnitName']['field'] = 'GoodUnitName';
/* Field: Vw ChkStock: BrchID */
$handler->display->display_options['fields']['BrchID']['id'] = 'BrchID';
$handler->display->display_options['fields']['BrchID']['table'] = 'vw_chkStock';
$handler->display->display_options['fields']['BrchID']['field'] = 'BrchID';
$handler->display->display_options['fields']['BrchID']['label'] = '';
$handler->display->display_options['fields']['BrchID']['exclude'] = TRUE;
$handler->display->display_options['fields']['BrchID']['element_label_colon'] = FALSE;
/* Field: Vw ChkStock: GoodGroupID */
$handler->display->display_options['fields']['GoodGroupID']['id'] = 'GoodGroupID';
$handler->display->display_options['fields']['GoodGroupID']['table'] = 'vw_chkStock';
$handler->display->display_options['fields']['GoodGroupID']['field'] = 'GoodGroupID';
$handler->display->display_options['fields']['GoodGroupID']['label'] = '';
$handler->display->display_options['fields']['GoodGroupID']['exclude'] = TRUE;
$handler->display->display_options['fields']['GoodGroupID']['element_label_colon'] = FALSE;
/* Field: Vw ChkStock: GoodGroupName */
$handler->display->display_options['fields']['GoodGroupName']['id'] = 'GoodGroupName';
$handler->display->display_options['fields']['GoodGroupName']['table'] = 'vw_chkStock';
$handler->display->display_options['fields']['GoodGroupName']['field'] = 'GoodGroupName';
$handler->display->display_options['fields']['GoodGroupName']['label'] = '������Թ��� :';
$handler->display->display_options['fields']['GoodGroupName']['exclude'] = TRUE;
/* Field: Global: PHP */
$handler->display->display_options['fields']['php']['id'] = 'php';
$handler->display->display_options['fields']['php']['table'] = 'views';
$handler->display->display_options['fields']['php']['field'] = 'php';
$handler->display->display_options['fields']['php']['label'] = '';
$handler->display->display_options['fields']['php']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['php']['use_php_setup'] = 0;
$handler->display->display_options['fields']['php']['php_output'] = '<?php


echo "<a href=\'/emgoodorder-edit/$row->ListNo/$row->InveID/$row->BrchID/$row->GoodID/$row->DeptID\'><p>���</p></a>";

?>';
$handler->display->display_options['fields']['php']['use_php_click_sortable'] = '0';
$handler->display->display_options['fields']['php']['php_click_sortable'] = '';
/* Field: Vw ChkStock: ListNo */
$handler->display->display_options['fields']['ListNo']['id'] = 'ListNo';
$handler->display->display_options['fields']['ListNo']['table'] = 'vw_chkStock';
$handler->display->display_options['fields']['ListNo']['field'] = 'ListNo';
$handler->display->display_options['fields']['ListNo']['label'] = '';
$handler->display->display_options['fields']['ListNo']['exclude'] = TRUE;
$handler->display->display_options['fields']['ListNo']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['ListNo']['separator'] = '';
/* Field: Vw ChkStock: InveID */
$handler->display->display_options['fields']['InveID']['id'] = 'InveID';
$handler->display->display_options['fields']['InveID']['table'] = 'vw_chkStock';
$handler->display->display_options['fields']['InveID']['field'] = 'InveID';
$handler->display->display_options['fields']['InveID']['exclude'] = TRUE;
/* Field: Vw ChkStock: GoodID */
$handler->display->display_options['fields']['GoodID']['id'] = 'GoodID';
$handler->display->display_options['fields']['GoodID']['table'] = 'vw_chkStock';
$handler->display->display_options['fields']['GoodID']['field'] = 'GoodID';
$handler->display->display_options['fields']['GoodID']['exclude'] = TRUE;
/* Field: Vw ChkStock: DeptID */
$handler->display->display_options['fields']['DeptID']['id'] = 'DeptID';
$handler->display->display_options['fields']['DeptID']['table'] = 'vw_chkStock';
$handler->display->display_options['fields']['DeptID']['field'] = 'DeptID';
$handler->display->display_options['fields']['DeptID']['exclude'] = TRUE;
/* Contextual filter: Vw ChkStock: BrchID */
$handler->display->display_options['arguments']['BrchID']['id'] = 'BrchID';
$handler->display->display_options['arguments']['BrchID']['table'] = 'vw_chkStock';
$handler->display->display_options['arguments']['BrchID']['field'] = 'BrchID';
$handler->display->display_options['arguments']['BrchID']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['BrchID']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['BrchID']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['BrchID']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['BrchID']['break_phrase'] = TRUE;
/* Contextual filter: Vw ChkStock: GoodGroupID */
$handler->display->display_options['arguments']['GoodGroupID']['id'] = 'GoodGroupID';
$handler->display->display_options['arguments']['GoodGroupID']['table'] = 'vw_chkStock';
$handler->display->display_options['arguments']['GoodGroupID']['field'] = 'GoodGroupID';
$handler->display->display_options['arguments']['GoodGroupID']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['GoodGroupID']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['GoodGroupID']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['GoodGroupID']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['GoodGroupID']['break_phrase'] = TRUE;
/* Filter criterion: Vw ChkStock: GoodCode */
$handler->display->display_options['filters']['GoodCode']['id'] = 'GoodCode';
$handler->display->display_options['filters']['GoodCode']['table'] = 'vw_chkStock';
$handler->display->display_options['filters']['GoodCode']['field'] = 'GoodCode';
$handler->display->display_options['filters']['GoodCode']['operator'] = 'contains';
$handler->display->display_options['filters']['GoodCode']['exposed'] = TRUE;
$handler->display->display_options['filters']['GoodCode']['expose']['operator_id'] = 'GoodCode_op';
$handler->display->display_options['filters']['GoodCode']['expose']['label'] = 'GoodCode';
$handler->display->display_options['filters']['GoodCode']['expose']['operator'] = 'GoodCode_op';
$handler->display->display_options['filters']['GoodCode']['expose']['identifier'] = 'GoodCode';
$handler->display->display_options['filters']['GoodCode']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
);
/* Filter criterion: Vw ChkStock: GoodName1 */
$handler->display->display_options['filters']['GoodName1']['id'] = 'GoodName1';
$handler->display->display_options['filters']['GoodName1']['table'] = 'vw_chkStock';
$handler->display->display_options['filters']['GoodName1']['field'] = 'GoodName1';
$handler->display->display_options['filters']['GoodName1']['operator'] = 'starts';
$handler->display->display_options['filters']['GoodName1']['exposed'] = TRUE;
$handler->display->display_options['filters']['GoodName1']['expose']['operator_id'] = 'GoodName1_op';
$handler->display->display_options['filters']['GoodName1']['expose']['label'] = 'GoodName1';
$handler->display->display_options['filters']['GoodName1']['expose']['operator'] = 'GoodName1_op';
$handler->display->display_options['filters']['GoodName1']['expose']['identifier'] = 'GoodName1';
$handler->display->display_options['filters']['GoodName1']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
$handler->display->display_options['path'] = 'check-stock';
