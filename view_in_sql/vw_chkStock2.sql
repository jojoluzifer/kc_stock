select	
(EMComp.CompID) CompID ,
(EMComp.CompName) CompName ,
(EMComp.CompCode) CompCode, 
(EMComp.CompOwner)CompOwner, 
(EMComp.TaxID) TaxID, 
(EMComp.CompNameEng)CompNameEng, 
(EMComp.CompOwnerEng)CompOwnerEng,
(EMGood.GoodCode) GoodCode ,
(EMGood.GoodID)GoodID,
(EMGood.GoodName1) GoodName1 ,
(EMGood.GoodNameEng1) GoodNameEng1 ,
(EMGoodUnit.GoodUnitCode) GoodUnitCode ,
(EMGoodUnit.GoodUnitName) GoodUnitName ,
(EMGoodUnit.GoodUnitNameEng) GoodUnitNameEng ,
(EMGoodGroup.GoodGroupID)GoodGroupID,
(EMGoodGroup.GoodGroupName)GoodGroupName,
(EMInve.InveCode)InveCode,
(EMInve.InveName)InveName,
(EMInve.InveNameEng)InveNameEng,
(ICCostDetail.BrchID)BrchID,
(ICCostDetail.DateOut)DateOut,
(EMGoodOrder.ListNo)ListNo,
(EMGoodOrder.MaxPoint)MaxPoint,
(EMInve.InveID),
(EMGoodOrder.StartDate)StartDate,
(EMGoodOrder.EndDate)EndDate,
sum(isnull(iccostdetail.remaqty,0))remaqty,
cast(sum(iccostdetail.remaamnt) as decimal(12,4)) /cast(sum(iccostdetail.remaqty) as decimal(12,4)) REmaCost , 
sum(isnull(iccostdetail.remaamnt,0))remaamnt, 
(EMGoodOrder.MinPoint)MinPoint,
--iif(remaamnt<EMGoodOrder.MinPoint,0,1) as flag
sum(isnull(iccostdetail.remaamnt,0))-EMGoodOrder.MinPoint as diff

from emgood	
Left Outer Join EMGoodUnit on (EMGood.MainGoodUnitID = EMGoodUnit.GoodUnitID)
Left Outer JoIn EMGoodType On (EMGoodType.GoodTypeID = EMGood.GoodTypeID )
Left Outer JoIn EMGoodCate On (EMGoodCate.GoodCateID = EMGood.GoodCateID )
Left Outer JoIn EMGoodGroup On (EMGoodGroup.GoodGroupID = EMGood.GoodGroupID )
Left Outer JoIn EMGoodBrand On (EMGoodBrand.GoodBrandID = EMGood.GoodBrandID ),
iccostdetail
left outer join eminve on eminve.inveid=iccostdetail.inveid
left join EMGoodOrder on EMGoodOrder.GoodID = ICCostDetail.GoodID,
emcomp

where	iccostdetail.goodid=emgood.goodid
and iccostdetail.dateout=(select max(dt.dateout) from iccostdetail dt where (dt.goodid=iccostdetail.goodid)
and (dt.inveid=eminve.inveid) 
and (dt.stockflag=-9)
and (dt.brchid=iccostdetail.brchid)
--and (dt.dateout<='20130101')
)
and iccostdetail.stockflag=-9
and iccostdetail.remaqty<>0
and (EMGood.GoodTypeflag not in ('S','E')) 
--and (ICCostDetail.brchid = 1 )
and iccostdetail.inveid is not null
group by
(EMComp.CompID) ,
(EMComp.CompName) ,
(EMComp.CompCode) , 
(EMComp.CompOwner), 
(EMComp.TaxID) , 
(EMComp.CompNameEng), 
(EMComp.CompOwnerEng),
(EMGood.GoodCode) ,
(EMGood.GoodName1) ,
(EMGood.GoodNameEng1) ,
(EMGoodUnit.GoodUnitCode) ,
(EMGoodUnit.GoodUnitName) ,
(EMGoodUnit.GoodUnitNameEng) ,
(EMInve.InveCode),
(EMInve.InveName),
(EMInve.InveNameEng),
(EMInve.InveID),
(EMGoodOrder.MinPoint),
ICCostDetail.BrchID,
ICCostDetail.DateOut,
EMGoodGroup.GoodGroupID,
EMGoodGroup.GoodGroupName,
EMGood.GoodID,
(EMGoodOrder.ListNo),
(EMGoodOrder.MaxPoint),
(EMGoodOrder.StartDate),
(EMGoodOrder.EndDate)