<?php


function absence_all_form($form, &$form_state) {
    $form['filter'] = array(
        '#type' => 'fieldset',
        '#title' => t('ค้นหา'),
        '#weight' => 0,
    );


    $form['filter']['TermYear'] = array(
        '#type' => 'select',
        '#prefix' => '<div id="dropdown-class-replace">',
        '#suffix' => '</div>',
        '#title' => t('ปีการศึกษา'),
        '#options' => _get_option_termyear_ansence(),
        '#default_value' => 0,
    );
    $form['filter']['TypeAbsence'] = array(
        '#type' => 'select',
        '#title' => t('ประเภทการลา'),
        '#options' => array(0 => "- เลือกประเภทการลา -", 4 => "ลาป่วย", 5 => "ลากิจ"),
        '#default_value' => 0,
    );
    $form['filter']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('ค้นหา'),
    );


    if ($form_state['rebuild']) {
        $form_state['values']['TermYear'];
        $form['result'] = array(
            '#type' => 'fieldset',
            '#title' => t('ผลลัพธ์'),
            '#weight' => 1,
        );
    }


    return $form;
}

function absence_all_form_submit($form, &$form_state){
    dsm($form_state);
    $form_state['rebuild'] = true;
}