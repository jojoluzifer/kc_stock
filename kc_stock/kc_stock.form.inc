<?php

function kc_stock_form($form, &$form_state, $variable, $groupArr) {

    drupal_add_css(drupal_get_path('module', 'kc_stock') . '/css/bootstrap.min.css');

    $form['#theme'] = 'kc_stock_theme';


    $form['name']['first'] = array(
        '#type' => 'textfield',
    );

    $form['min'] = array(
        '#type' => 'select',
        '#options' => array(
            0 => t('กรุณาเลือก'),
            1 => t('น้อยกว่าค่าคงเหลือ'),
            2 => t('มากกว่าค่าคงเหลือ'),
        ),
        '#default_value' => 0,
    );
    $form['group'] = array(
        '#type' => 'select',
        '#options' => get_groupName(),
        '#default_value' => 0,
    );
    $form['search'] = array(
        '#type' => 'submit',
        '#value' => t('ค้นหา'),
    );




    if ($form_state['rebuild']) {

        $form_state['rebuild'] = FALSE;


        $form['middle'] = array(
            "#markup" => kc_check_stock($variable, array($form_state['input']['first'], $form_state['input']['min'], $form_state['input']['group'])));
    } else {

        $form['middle'] = array(
            "#markup" => kc_check_stock($variable, 'search')
        );
    }


    return $form;
}

function kc_stock_theme() {
    return array
        (
        'kc_stock_theme' => array
            (
            'render element' => 'form'
        ),
    );
}

function theme_kc_stock_theme($variables) {
    // Isolate the form definition form the $variables array
    $form = $variables['form'];
    $output = '<div><label class="badge badge-info" >' . t('ค้นหา') . '</label></div>';
    // Put the entire structure into a div that can be used for
    // CSS purposes

    $output .= '<div id="row-fluid" style="margin-top:15px;">';

    $output .= '<div class="row-fluid">';
    $output .= '<div class="span5">';
    $output .= '<div class="row-fluid">';
    $output .= '<div class="span4">';
    $output .= '<div><p style="margin-top:7px;">รหัสสินค้า :</p></div>';
    $output .= ' </div>';
    $output .= ' <div class="span6">';
    $output .= drupal_render($form["name"]["first"]);
    $output .= ' </div>';
    $output .= '</div>';
    $output .= ' </div>';
    $output .= ' <div class="span6">';
    $output .= '<div class="row-fluid">';
    $output .= '<div class="span4">';
    $output .= '<div><p style="margin-top:7px;">ค่าคงเหลือ :</p></div>';
    $output .= ' </div>';
    $output .= ' <div class="span6">';
    $output .= drupal_render($form["min"]);
    $output .= ' </div>';
    $output .= '</div>';
    $output .= '</div>';
    $output .= '<div class="span1">';
    $output .= drupal_render($form["search"]);
    $output .= '</div>';
    $output .= '</div>';

    $output .= '<div id="row-fluid" style="margin-top:10px;">';

    $output .= '<div class="row-fluid">';
    $output .= '<div class="span5">';
    $output .= '<div class="row-fluid">';
    $output .= '<div class="span4">';
    $output .= '<div><p style="margin-top:7px;">กลุ่มสินค้า :</p></div>';
    $output .= ' </div>';
    $output .= ' <div class="span7">';
    $output .= drupal_render($form["group"]);
    $output .= ' </div>';
    $output .= '</div>';
    $output .= ' </div>';
    $output .= ' <div class="span6">';
    $output .= '<div class="row-fluid">';
    $output .= '<div class="span4">';
//    $output .= '<div><p style="margin-top:7px;">ค่าคงเหลือ :</p></div>';
    $output .= ' </div>';
    $output .= ' <div class="span6">';
    $output .= drupal_render($form["min"]);
    $output .= ' </div>';
    $output .= '</div>';
    $output .= '</div>';
    $output .= '</div>';


    // Pass the remaining form elements through drupal_render_children()
    $output .= drupal_render_children($form);
    // return the output
    return $output;
}

function kc_stock_form_submit($form, &$form_state) {




    $form_state['rebuild'] = true;
}

function option_EMVATGroup() {
    $sql = "SELECT GoodGroupID, GoodGroupName FROM EMGoodGroup"; // WhERE GoodGroupID <'1015' ";
    $result = db_query($sql)->fetchAll();
    $array = array();
    // dsm($result);
    foreach ($result as $value) {
        $array[$value->GoodGroupID] = $value->GoodGroupName . " ->" . $value->GoodGroupID;
        // $array[$value->GoodGroupID] = $value->GoodGroupName . " ->" . $value->GoodGroupID;
    }
    //dsm($array);
    return $array;
}

function group_form($form, &$form_state, $brchid) {
    $form['BrchID'] = array(
        '#type' => 'hidden',
        '#default_value' => $brchid,
    );

    $form['check_type'] = array(
        '#type' => 'radios',
        '#options' => array(
            '1' => t('เลือกจากกลุ่มสินค้า'),
            '2' => t('เลือกเฉพาะกลุ่มสินค้า'),
        ),
        '#default_value' => "1",
        '#title' => t('เลือกแบบ ?')
    );
    $form['option_EMGroup_fieldset'] = array(
        '#type' => 'fieldset',
        '#title' => t('เลือกจากกลุ่มสินค้า'),
        //      '#collapsible' => TRUE,
        //     '#collapsed' => TRUE,
        '#states' => array(
            'visible' => array(
                ':input[name="check_type"]' => array('value' => '1'),
            ),
        ),
    );

    $form['option_EMGroup_fieldset']['GoodGroupID_option_start'] = array(
        '#type' => 'select',
        '#title' => t('จาก กลุ่มสินค้า'),
        '#options' => option_EMVATGroup(),
            //   '#default_value' => $category['selected'],
    );

    $form['option_EMGroup_fieldset']['GoodGroupID_option_end'] = array(
        '#type' => 'select',
        '#title' => t('ถึง '),
        '#options' => option_EMVATGroup(),
    );



    $form['checkbox_EMGroup_fieldset'] = array(
        '#type' => 'fieldset',
        '#title' => t('เลือกเฉพาะกลุ่มสินค้า'),
        //       '#collapsible' => TRUE,
        //      '#collapsed' => TRUE,
        '#states' => array(
            'visible' => array(
                ':input[name="check_type"]' => array('value' => '2'),
            ),)
    );
    $form['checkbox_EMGroup_fieldset']['GoodGroupID_checkbox'] = array(
        '#type' => 'checkboxes',
        '#options' => option_EMVATGroup(),
            //     '#title' => t('What standardized tests did you take?'),
    );


    $form['submit'] = array('#type' => 'submit',
        '#value' => t('เลือก'));

    return $form;
}

function group_form_submit($form, &$form_state) {
    $values = $form_state['values'];
    $check_from_list = '';
    $check_list = '';
    $option_start = $values['GoodGroupID_option_start'];
    $option_end = $values['GoodGroupID_option_end'];
//      dsm($option_start);
//       dsm($option_end);
    if ($values['check_type'] == 1) {

        $sql = "SELECT GoodGroupID FROM EMGoodGroup where GoodGroupID >='$option_start' and GoodGroupID <= '$option_end'";
        $result1 = db_query($sql)->fetchAll();
        $array = array();
        // dsm($result);
        foreach ($result1 as $value) {
            $check_from_list .= $value->GoodGroupID . ",";
        }

        $check_from_list = substr($check_from_list, 0, -1);
        //   dsm($check_from_list);
        drupal_goto('check-stock/' . $values['BrchID'] . "/" . $check_from_list);
    }
    if ($values['check_type'] == 2) {
        foreach ($values['GoodGroupID_checkbox'] as $key => $line) {
            if ($line != 0) {
                $check_list .= $key . ",";
            }
        }

        $check_list = substr($check_list, 0, -1);
        drupal_goto('check-stock/' . $values['BrchID'] . "/" . $check_list);
        //     dsm($check_list);
    }
}

function group2_form($form, &$form_state, $brchid) {
    $form['BrchID'] = array(
        '#type' => 'hidden',
        '#default_value' => $brchid,
    );

    $form['check_type'] = array(
        '#type' => 'radios',
        '#options' => array(
            '1' => t('เลือกจากกลุ่มสินค้า'),
            '2' => t('เลือกเฉพาะกลุ่มสินค้า'),
        ),
        '#default_value' => "1",
        '#title' => t('เลือกแบบ ?')
    );
    $form['option_EMGroup_fieldset'] = array(
        '#type' => 'fieldset',
        '#title' => t('เลือกจากกลุ่มสินค้า'),
        //      '#collapsible' => TRUE,
        //     '#collapsed' => TRUE,
        '#states' => array(
            'visible' => array(
                ':input[name="check_type"]' => array('value' => '1'),
            ),
        ),
    );

    $form['option_EMGroup_fieldset']['GoodGroupID_option_start'] = array(
        '#type' => 'select',
        '#title' => t('จาก กลุ่มสินค้า'),
        '#options' => option_EMVATGroup(),
            //   '#default_value' => $category['selected'],
    );

    $form['option_EMGroup_fieldset']['GoodGroupID_option_end'] = array(
        '#type' => 'select',
        '#title' => t('ถึง '),
        '#options' => option_EMVATGroup(),
    );



    $form['checkbox_EMGroup_fieldset'] = array(
        '#type' => 'fieldset',
        '#title' => t('เลือกเฉพาะกลุ่มสินค้า'),
        //       '#collapsible' => TRUE,
        //      '#collapsed' => TRUE,
        '#states' => array(
            'visible' => array(
                ':input[name="check_type"]' => array('value' => '2'),
            ),)
    );
    $form['checkbox_EMGroup_fieldset']['GoodGroupID_checkbox'] = array(
        '#type' => 'checkboxes',
        '#options' => option_EMVATGroup(),
            //     '#title' => t('What standardized tests did you take?'),
    );


    $form['submit'] = array('#type' => 'submit',
        '#value' => t('เลือก'));

    return $form;
}

function group2_form_submit($form, &$form_state) {
    $values = $form_state['values'];
    $check_from_list = '';
    $check_list = '';
    $option_start = $values['GoodGroupID_option_start'];
    $option_end = $values['GoodGroupID_option_end'];
//      dsm($option_start);
//       dsm($option_end);
    if ($values['check_type'] == 1) {

        $sql = "SELECT GoodGroupID FROM EMGoodGroup where GoodGroupID >='$option_start' and GoodGroupID <= '$option_end'";
        $result1 = db_query($sql)->fetchAll();
        $array = array();
        // dsm($result);
        foreach ($result1 as $value) {
            $check_from_list .= $value->GoodGroupID . ",";
        }

        $check_from_list = substr($check_from_list, 0, -1);
        //   dsm($check_from_list);
        drupal_goto('check-stock-all/' . $values['BrchID'] . "/" . $check_from_list);
    }
    if ($values['check_type'] == 2) {
        foreach ($values['GoodGroupID_checkbox'] as $key => $line) {
            if ($line != 0) {
                $check_list .= $key . ",";
            }
        }

        $check_list = substr($check_list, 0, -1);
        drupal_goto('check-stock-all/' . $values['BrchID'] . "/" . $check_list);
        //     dsm($check_list);
    }
}