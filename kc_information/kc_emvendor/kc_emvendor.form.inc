<?php
function kc_emvendor_form($form,&$form_state,$id){
	$values_null = array('AccID','VendorGroupID','CurrID','ShortName','CreditAmnt',
			'VendorStartDate','Inactive','InactiveDate','CreditTermType','GoodDisc',
			'EditGoodDisc','BillDisc','EditBillDisc','VendorDesc','BillState',
			'BillDay','BillDate','BillWeek','BillRemark','PayState',
			'PayDay','PayDate','PayWeek','PayRemark','Capital',
			'ThaiOwner','FognPercent','ThaiPercent','Decision','ManagStyl',
			'TurnOver','NoEmpOffice','NoEmpFac','Sex','APID',
			'behalfname','behalfaddr1','behalfaddr2','behalfdistrict','behalfamphur',
			'behalfprovince','behalfpostcode','locacharge');
	$options_CreditDays = _kc_emvendor_options_CreditDays();
	$options_VendorType = _kc_emvendor_options_VendorType();
	$options_VendorTitle = _kc_emvendor_options_VendorTitle();
	$options_VATGroupID = _kc_emvendor_options_VATGroupID();
	$options_EMBrch = _kc_emvendor_options_EMBrch();
	$options_EMVendorType = _kc_emvendor_options_EMVendorType();
	$options_EMBank = _kc_emvendor_options_EMBank();
	$options_locacharge = _kc_emvendor_options_locacharge();
	$form['job'] = array('#type' => 'hidden', '#value' => $id);
	$form['values_null'] = array('#type' => 'hidden', '#value' => $values_null);
	$form['#tree'] = TRUE;
	$form['information'] = array(
			'#type' => 'fieldset',
			'#title' => t('ข้อมูลผู้ขาย'),
			'#collapsible' => TRUE,
			'#collapsed' => FALSE,
	);
	$form['information']['VendorID'] = array(
			'#type' => 'textfield',
			'#title' => t('ไอดี ผู้ขาย'),
			'#disabled' => TRUE,
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	$form['information']['VendorCode'] = array(
			'#type' => 'textfield',
			'#title' => t('รหัส ผู้ขาย'),
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	$form['information']['VendorTitle'] = array(
			'#type' => 'select',
			'#title' => t('คำนำหน้า ผู้ขาย'),
			'#options' =>$options_VendorTitle
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	$form['information']['VendorName'] = array(
			'#type' => 'textfield',
			'#title' => t('ชื่อ ผู้ขาย ภาษาไทย'),
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	$form['information']['VendorNameEng'] = array(
			'#type' => 'textfield',
			'#title' => t('ชื่อ ผู้ขาย ภาษาอังกฤษ'),
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	$form['information']['ShortName'] = array(
			'#type' => 'textfield',
			'#title' => t('ชื่อย่อ ผู้ขาย'),
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);

	$form['address1'] = array(
			'#type' => 'fieldset',
			'#title' => t('ที่อยู่ ภพ.20'),
			'#collapsible' => TRUE,
			'#collapsed' => FALSE,
	);
	$form['address1']['VendorAddr1'] = array(
			'#type' => 'textfield',
			'#title' => t('ที่อยู่'),
	);
	$form['address1']['VendorAddr2'] = array(
			'#type' => 'textfield',
	);
	$form['address1']['District'] = array(
			'#type' => 'textfield',
			'#title' => t('แขวง/ตำบล'),
	);
	$form['address1']['Amphur'] = array(
			'#type' => 'textfield',
			'#title' => t('เขต/อำเภอ'),
	);
	$form['address1']['Province'] = array(
			'#type' => 'textfield',
			'#title' => t('จังหวัด'),
	);
	$form['address1']['PostCode'] = array(
			'#type' => 'textfield',
			'#title' => t('รหัสไปรษณีย์'),
	);
	$form['address2'] = array(
			'#type' => 'fieldset',
			'#title' => t('ที่อยู่ ที่ติดต่อ'),
			'#collapsible' => TRUE,
			'#collapsed' => FALSE,
	);

	$form['address2']['ContAddr1'] = array(
			'#type' => 'textfield',
			'#title' => t('ที่อยู่'),
	);
	$form['address2']['ContAddr2'] = array(
			'#type' => 'textfield',
	);
	$form['address2']['ContDistrict'] = array(
			'#type' => 'textfield',
			'#title' => t('แขวง/ตำบล'),
	);
	$form['address2']['ContAmphur'] = array(
			'#type' => 'textfield',
			'#title' => t('เขต/อำเภอ'),
	);
	$form['address2']['ContProvince'] = array(
			'#type' => 'textfield',
			'#title' => t('จังหวัด'),
	);
	$form['address2']['ContPostCode'] = array(
			'#type' => 'textfield',
			'#title' => t('รหัสไปรษณีย์'),
	);
	$form['address2']['ContTel'] = array(
			'#type' => 'textfield',
			'#title' => t('เบอร์โทรศัพท์'),
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	$form['address2']['ContTelExtend1'] = array(
			'#type' => 'textfield',
			'#title' => t('ต่อ'),
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	$form['address2']['ContFax'] = array(
			'#type' => 'textfield',
			'#title' => t('เบอร์แฟกซ์'),
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	$form['address2']['ContEMail'] = array(
			'#type' => 'textfield',
			'#title' => t('E-mail'),
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	$form['address2']['ContHomePage'] = array(
			'#type' => 'textfield',
			'#title' => t('Home Page'),
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	$form['detail'] = array(
			'#type' => 'fieldset',
			'#title' => t('รายละเอียด'),
			'#collapsible' => TRUE,
			'#collapsed' => FALSE,
	);


	$form['detail']['VendorType'] = array(
			'#type' => 'select',
			'#title' => t('ชนิดประเภท ผู้ขาย'),
			'#options' => $options_VendorType,
	);
	$form['detail']['CreditDays'] = array(
			'#type' => 'select',
			'#title' => t('เครดิต (วัน)'),
			'#options' => $options_CreditDays,
	);
	$form['detail']['TaxId'] = array(
			'#type' => 'textfield',
			'#title' => t('เลขประจำตัวผู้เสียภาษี'),
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	$form['detail']['CapitalNo'] = array(
			'#type' => 'textfield',
			'#title' => t('เลขที่จดทะเบียน'),
	);
	$form['detail']['CardNo'] = array(
			'#type' => 'textfield',
			'#title' => t('เลขที่บัตรประชาชน'),
	);
	$form['detail']['VATGroupID'] = array(
			'#type' => 'select',
			'#title' => t('กลุ่มภาษี'),
			'#options' => $options_VATGroupID,
	);
	$form['detail']['BrchID'] = array(
			'#type' => 'select',
			'#title' => t('สาขา'),
			'#options' => $options_EMBrch,
	);
	$form['detail']['VendorTypeID'] = array(
			'#type' => 'select',
			'#title' => t('ประเภท ผู้ขาย'),
			'#options' => $options_EMVendorType,
	);
	$form['detail']['Birthdate'] = array(
			'#type' => 'date',
			'#title' => t('วันเกิดผู้ขาย'),
			//			'#options' => $options_EMVendorType,
	);
	$form['detail']['BankID'] = array(
			'#type' => 'select',
			'#title' => t('ธนาคาร ผู้ขาบ'),
			'#options' => $options_EMBank,
	);
	$form['detail']['BranchCode'] = array(
			'#type' => 'textfield',
			'#title' => t('สาขาธนาคาร'),
	);
	$form['detail']['BookNo'] = array(
			'#type' => 'textfield',
			'#title' => t('เลขที่บัญชีผู้ขาย'),
	);
	/*
	 $form['detail']['locacharge']  = array(
	 		'#type' => 'select',
	 		'#title' => t('Charge To:'),
	 		'#options' => $options_locacharge,
	 		'#description' => 'SCBT Only',
	 );
	/*
	$form['information']['Addr21'] = array(
			'#type' => 'textfield',
			'#title' => t('ที่อยู่ '),
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	$form['information']['District'] = array(
			'#type' => 'textfield',
			'#title' => t('ตำบล'),
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	$form['information']['Amphur'] = array(
			'#type' => 'textfield',
			'#title' => t('อำเภอ'),
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	$form['information']['Province'] = array(
			'#type' => 'textfield',
			'#title' => t('จังหวัด'),
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	$form['information']['PostCode'] = array(
			'#type' => 'textfield',
			'#title' => t('รหัสไปรษณีย์'),
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	$form['information']['Tel'] = array(
			'#type' => 'textfield',
			'#title' => t('เบอร์โทรศัพท์'),
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	$form['information']['Fax'] = array(
			'#type' => 'textfield',
			'#title' => t('เบอร์แฟกซ์'),
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	*/
	$form['submit'] = array(
			'#type' => 'submit',
			'#value' => t('บันทึก'));
	///
	if ($id!='add'){
		$emreceive_place_values = kc_emvendor_get_data($id);
		foreach ($emreceive_place_values as $key=>$values){
			if (!(in_array($key, $values_null))){
				if ($key=='Birthdate'){
						
					dsm($values);
					list($year, $month, $day) = explode('-', $values);
					$day = explode(' ', $day);
					$def = array('year' => $year, 'month' =>  $month+0, 'day' => $day[0]);
					$form['detail'][$key]['#default_value'] = $def;
				}else{
					$form['information'][$key]['#default_value'] = $values;
					$form['address1'][$key]['#default_value'] = $values;
					$form['address2'][$key]['#default_value'] = $values;
					$form['detail'][$key]['#default_value'] = $values;
				}
			}
		}
	}
	return $form;
}
function kc_emvendor_form_submit($form,&$form_state){
	//
	$form_state['rebuild'] = TRUE;
	$values = $form_state['values'];
	$emvendor = array_merge($values['information'],$values['address1'],$values['address2'],$values['detail']);
	//$emvendor = array_merge($values['information']);
	$emvendor['CreditAmnt'] = 0.00;
	$emvendor['Inactive'] = 'A';
	$emvendor['EditGoodDisc'] = "N";
	$emvendor['EditBillDisc'] = 'N';
	$emvendor['ThaiOwner'] = 'N';
	$emvendor['FognPercent'] = 0;
	$emvendor['ThaiPercent'] = 0;
	$emvendor['BillDay'] ='';
	$emvendor['BillDate']='';
	$emvendor['BillWeek']='';
	$emvendor['Birthdate'] = $emvendor['Birthdate']['year'].'-'.$emvendor['Birthdate']['month'].'-'.$emvendor['Birthdate']['day'].' 00:00:00.0';
	//dsm($form_state);
	//dsm($values);
	//($emvendor);
	if ($values['job']=='add'){
		kc_emvendor_add_data($emvendor);
	}else{
		$emvendor_in_base = kc_emvendor_get_data($emvendor['VendorID']);
		foreach ($values['values_null'] as $key){
			unset($emvendor_in_base[$key]);
		}
		$arr = array_diff($emvendor_in_base, $emvendor);
		dsm(count($arr));
		dsm($arr);
		dsm($emvendor);
		if (count($arr)>0){
			$aaa = kc_emvendor_update_data($emvendor);
			drupal_set_message(t("ได้ Update แล้ว"));
		}else{
			drupal_set_message(t("ไม่มีข้อมูลเรียนแปลง"), 'warning');
		}
	}
	drupal_goto("/kc_emvendor/view");


}
function _kc_emvendor_options_CreditDays(){
	$temp = db_query('SELECT DISTINCT CreditDays FROM {EMVendor} ORDER BY CreditDays ASC')->fetchCol();
	return drupal_map_assoc($temp);
	//return drupal_map_assoc(array(0, 15, 30, 60));
}
function _kc_emvendor_options_VendorType(){
	return array(
			1 => 'นิติบุคคล',
			2 => 'บุคคลธรรมดา');
}
function _kc_emvendor_options_VendorTitle(){
	$temp = db_query('SELECT DISTINCT VendorTitle FROM {EMVendor} ORDER BY VendorTitle ASC')->fetchCol();
	return drupal_map_assoc($temp);
}
function _kc_emvendor_options_VATGroupID(){
	return db_query('SELECT VATGroupID, VATGroupCode FROM {EMVATGroup} ORDER BY VATGroupID ASC')->fetchAllKeyed();
}
function _kc_emvendor_options_EMBrch(){
	return db_query('SELECT BrchID, BrchName FROM {EMBrch} ORDER BY BrchID ASC')->fetchAllKeyed();
}
function _kc_emvendor_options_EMVendorType(){
	return db_query('SELECT VendorTypeID, VendorTypeName FROM {EMVendorType} ORDER BY VendorTypeID ASC')->fetchAllKeyed();
}
function _kc_emvendor_options_EMBank(){
	return db_query('SELECT BankID, BankName FROM {EMBank} ORDER BY BankID ASC')->fetchAllKeyed();
}
function _kc_emvendor_options_locacharge(){
	return array(
			0=> NULL,
			'C' => 'Customer',
			'P' => 'Vendor'
	);
}
function kc_emvendor_add_data($data){

	$ret = db_query('select max(VendorID)+1 from EMVendor')->fetchCol();
	$id = $ret[0];
	$data['VendorID'] = $id;



	$data['VendorCode'] = kc_emvendor_get_Next_VendoeCode($data['BrchID']);
	dsm($data);
	$emvendor = entity_create('EMVendor', $data);
	$emvendor->save();
	kc_emvendor_update_Next_VendoeCode($data['BrchID'],$data['VendorCode']);
	//	$sql = _arrayToSql('EMVendor', $data);
	//	dsm($sql);
	//	Select LastNo , RUNFORMAT from EMRunBrch Where BrchID =1 and RunCode ='vendor'
	//	$emreceive_place = entity_create('EMReceivePlace', $emreceive_place_values);
	//	$emreceive_place->save();
}

function kc_emvendor_update_Next_VendoeCode($BrchID,$LastCode){
	$num_updated = db_update('EMRunBrch') // Table name no longer needs {}
	->fields(array(
			'LastNo' => $LastCode,
	))
	->condition('BrchID', $BrchID,'=')
	->condition('RunCode', 'vendor','=')
	->execute();
}
function kc_emvendor_get_Next_VendoeCode($BrchID){
	$result = db_select('EMRunBrch', 'n')
	->fields('n',array('LastNo','RUNFORMAT'))
	->condition('BrchID', $BrchID,'=')
	->condition('RunCode', 'vendor','=')
	->execute()
	->fetchAssoc();
	$lastNo = explode('-',$result['LastNo']);
	$strlan = strlen($lastNo[1]);
	$nextNum = $lastNo[1]+1;
	$format = $lastNo[0].'-%0'.$strlan.'d';
	$nextCode = sprintf($format,$nextNum);
	return $nextCode;
}
function _arrayToSql($table, $array, $insert = "INSERT INTO") {

	//Check if user wants to insert or update
	if ($insert != "UPDATE") {
		$insert = "INSERT INTO";
	}

	$columns = array();
	$data = array();

	foreach ( $array as $key => $value) {
		$columns[] = $key;
		if ($value != "") {
			$data[] = "'" . $value . "'";
		} else {
			$data[] = "NULL";
		}

		//TODO: ensure no commas are in the values
	}

	$cols = implode(",",$columns);
	$values = implode(",",$data);

	$sql = <<<EOSQL
  $insert `$table`
  ($cols)
  VALUES
  ($values)
EOSQL;
	return $sql;

}
?>