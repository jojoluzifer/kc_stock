<?php
function kc_emvendor_views_default_views() {

	// exported view goes here
	$view = new view();
	$view->name = 'emvendor';
	$view->description = '';
	$view->tag = 'default';
	$view->base_table = 'EMVendor';
	$view->human_name = 'EMVendor';
	$view->core = 7;
	$view->api_version = '3.0';
	$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

	/* Display: Master */
	$handler = $view->new_display('default', 'Master', 'default');
	$handler->display->display_options['title'] = 'ข้อมูลผู้ขาย';
	$handler->display->display_options['use_more_always'] = FALSE;
	$handler->display->display_options['access']['type'] = 'none';
	$handler->display->display_options['cache']['type'] = 'none';
	$handler->display->display_options['query']['type'] = 'views_query';
	$handler->display->display_options['exposed_form']['type'] = 'basic';
	$handler->display->display_options['pager']['type'] = 'full';
	$handler->display->display_options['pager']['options']['items_per_page'] = '25';
	$handler->display->display_options['pager']['options']['offset'] = '0';
	$handler->display->display_options['pager']['options']['id'] = '0';
	$handler->display->display_options['pager']['options']['quantity'] = '9';
	$handler->display->display_options['style_plugin'] = 'table';
	/* Header: Global: PHP */
	$handler->display->display_options['header']['php']['id'] = 'php';
	$handler->display->display_options['header']['php']['table'] = 'views';
	$handler->display->display_options['header']['php']['field'] = 'php';
	$handler->display->display_options['header']['php']['php_output'] = '<?php
			echo "<a href=\'add\'>เพิ่ม </a>";
			?>
			';
	/* Field: EMVendor: Emvendor ID */
	$handler->display->display_options['fields']['VendorID']['id'] = 'VendorID';
	$handler->display->display_options['fields']['VendorID']['table'] = 'EMVendor';
	$handler->display->display_options['fields']['VendorID']['field'] = 'VendorID';
	$handler->display->display_options['fields']['VendorID']['exclude'] = TRUE;
	$handler->display->display_options['fields']['VendorID']['separator'] = '';
	/* Field: EMVendor: VendorCode */
	$handler->display->display_options['fields']['VendorCode']['id'] = 'VendorCode';
	$handler->display->display_options['fields']['VendorCode']['table'] = 'EMVendor';
	$handler->display->display_options['fields']['VendorCode']['field'] = 'VendorCode';
	$handler->display->display_options['fields']['VendorCode']['label'] = 'รหัสผู้ขาย';
	/* Field: EMVendor: VendorName */
	$handler->display->display_options['fields']['VendorName']['id'] = 'VendorName';
	$handler->display->display_options['fields']['VendorName']['table'] = 'EMVendor';
	$handler->display->display_options['fields']['VendorName']['field'] = 'VendorName';
	$handler->display->display_options['fields']['VendorName']['label'] = 'ชื่อผู้ขาย';
	/* Field: EMVendor: VendorAddr1 */
	$handler->display->display_options['fields']['VendorAddr1']['id'] = 'VendorAddr1';
	$handler->display->display_options['fields']['VendorAddr1']['table'] = 'EMVendor';
	$handler->display->display_options['fields']['VendorAddr1']['field'] = 'VendorAddr1';
	$handler->display->display_options['fields']['VendorAddr1']['exclude'] = TRUE;
	/* Field: EMVendor: VendorAddr2 */
	$handler->display->display_options['fields']['VendorAddr2']['id'] = 'VendorAddr2';
	$handler->display->display_options['fields']['VendorAddr2']['table'] = 'EMVendor';
	$handler->display->display_options['fields']['VendorAddr2']['field'] = 'VendorAddr2';
	$handler->display->display_options['fields']['VendorAddr2']['exclude'] = TRUE;
	/* Field: EMVendor: Amphur */
	$handler->display->display_options['fields']['Amphur']['id'] = 'Amphur';
	$handler->display->display_options['fields']['Amphur']['table'] = 'EMVendor';
	$handler->display->display_options['fields']['Amphur']['field'] = 'Amphur';
	$handler->display->display_options['fields']['Amphur']['exclude'] = TRUE;
	/* Field: EMVendor: District */
	$handler->display->display_options['fields']['District']['id'] = 'District';
	$handler->display->display_options['fields']['District']['table'] = 'EMVendor';
	$handler->display->display_options['fields']['District']['field'] = 'District';
	$handler->display->display_options['fields']['District']['exclude'] = TRUE;
	/* Field: EMVendor: PostCode */
	$handler->display->display_options['fields']['PostCode']['id'] = 'PostCode';
	$handler->display->display_options['fields']['PostCode']['table'] = 'EMVendor';
	$handler->display->display_options['fields']['PostCode']['field'] = 'PostCode';
	$handler->display->display_options['fields']['PostCode']['exclude'] = TRUE;
	/* Field: EMVendor: Province */
	$handler->display->display_options['fields']['Province']['id'] = 'Province';
	$handler->display->display_options['fields']['Province']['table'] = 'EMVendor';
	$handler->display->display_options['fields']['Province']['field'] = 'Province';
	$handler->display->display_options['fields']['Province']['exclude'] = TRUE;
	/* Field: Global: Custom text */
	$handler->display->display_options['fields']['nothing']['id'] = 'nothing';
	$handler->display->display_options['fields']['nothing']['table'] = 'views';
	$handler->display->display_options['fields']['nothing']['field'] = 'nothing';
	$handler->display->display_options['fields']['nothing']['label'] = 'ที่อยู่';
	$handler->display->display_options['fields']['nothing']['alter']['text'] = '[VendorAddr1] [VendorAddr2] [District] [Amphur] [Province] [PostCode]';
	/* Field: EMVendor: ContTel */
	$handler->display->display_options['fields']['ContTel']['id'] = 'ContTel';
	$handler->display->display_options['fields']['ContTel']['table'] = 'EMVendor';
	$handler->display->display_options['fields']['ContTel']['field'] = 'ContTel';
	$handler->display->display_options['fields']['ContTel']['label'] = 'โทรศัพท์';
	/* Field: EMVendor: ContFax */
	$handler->display->display_options['fields']['ContFax']['id'] = 'ContFax';
	$handler->display->display_options['fields']['ContFax']['table'] = 'EMVendor';
	$handler->display->display_options['fields']['ContFax']['field'] = 'ContFax';
	$handler->display->display_options['fields']['ContFax']['label'] = 'แฟ็กซ์';
	/* Field: EMVendor: CreditDays */
	$handler->display->display_options['fields']['CreditDays']['id'] = 'CreditDays';
	$handler->display->display_options['fields']['CreditDays']['table'] = 'EMVendor';
	$handler->display->display_options['fields']['CreditDays']['field'] = 'CreditDays';
	$handler->display->display_options['fields']['CreditDays']['label'] = 'เครดิต (วัน)';
	$handler->display->display_options['fields']['CreditDays']['hide_empty'] = TRUE;
	$handler->display->display_options['fields']['CreditDays']['separator'] = '';
	/* Field: Global: Custom text */
	$handler->display->display_options['fields']['nothing_1']['id'] = 'nothing_1';
	$handler->display->display_options['fields']['nothing_1']['table'] = 'views';
	$handler->display->display_options['fields']['nothing_1']['field'] = 'nothing';
	$handler->display->display_options['fields']['nothing_1']['label'] = '';
	$handler->display->display_options['fields']['nothing_1']['alter']['text'] = 'แก้ไข';
	$handler->display->display_options['fields']['nothing_1']['alter']['make_link'] = TRUE;
	$handler->display->display_options['fields']['nothing_1']['alter']['path'] = 'kc_emvendor/[VendorID]/edit';
	/* Sort criterion: EMVendor: VendorCode */
	$handler->display->display_options['sorts']['VendorCode']['id'] = 'VendorCode';
	$handler->display->display_options['sorts']['VendorCode']['table'] = 'EMVendor';
	$handler->display->display_options['sorts']['VendorCode']['field'] = 'VendorCode';
	/* Filter criterion: EMVendor: VendorCode */
	$handler->display->display_options['filters']['VendorCode']['id'] = 'VendorCode';
	$handler->display->display_options['filters']['VendorCode']['table'] = 'EMVendor';
	$handler->display->display_options['filters']['VendorCode']['field'] = 'VendorCode';
	$handler->display->display_options['filters']['VendorCode']['operator'] = 'word';
	$handler->display->display_options['filters']['VendorCode']['exposed'] = TRUE;
	$handler->display->display_options['filters']['VendorCode']['expose']['operator_id'] = 'VendorCode_op';
	$handler->display->display_options['filters']['VendorCode']['expose']['label'] = 'รหัสผู้ขาย';
	$handler->display->display_options['filters']['VendorCode']['expose']['operator'] = 'VendorCode_op';
	$handler->display->display_options['filters']['VendorCode']['expose']['identifier'] = 'VendorCode';
	$handler->display->display_options['filters']['VendorCode']['expose']['remember_roles'] = array(
			2 => '2',
			1 => 0,
			3 => 0,
			10 => 0,
			11 => 0,
			12 => 0,
			13 => 0,
			14 => 0,
			15 => 0,
			16 => 0,
			17 => 0,
			18 => 0,
			19 => 0,
			20 => 0,
			21 => 0,
			22 => 0,
			23 => 0,
	);
	/* Filter criterion: EMVendor: VendorName */
	$handler->display->display_options['filters']['VendorName']['id'] = 'VendorName';
	$handler->display->display_options['filters']['VendorName']['table'] = 'EMVendor';
	$handler->display->display_options['filters']['VendorName']['field'] = 'VendorName';
	$handler->display->display_options['filters']['VendorName']['operator'] = 'word';
	$handler->display->display_options['filters']['VendorName']['exposed'] = TRUE;
	$handler->display->display_options['filters']['VendorName']['expose']['operator_id'] = 'VendorName_op';
	$handler->display->display_options['filters']['VendorName']['expose']['label'] = 'ชื่อผู้ขาย';
	$handler->display->display_options['filters']['VendorName']['expose']['operator'] = 'VendorName_op';
	$handler->display->display_options['filters']['VendorName']['expose']['identifier'] = 'VendorName';
	$handler->display->display_options['filters']['VendorName']['expose']['remember_roles'] = array(
			2 => '2',
			1 => 0,
			3 => 0,
			10 => 0,
			11 => 0,
			12 => 0,
			13 => 0,
			14 => 0,
			15 => 0,
			16 => 0,
			17 => 0,
			18 => 0,
			19 => 0,
			20 => 0,
			21 => 0,
			22 => 0,
			23 => 0,
	);

	/* Display: Page */
	$handler = $view->new_display('page', 'Page', 'page');
	$handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
	$handler->display->display_options['path'] = 'kc_emvendor/view';




	$views[$view->name] = $view;
	// return views
	return $views;
}
?>