<?php
function kc_emreceive_place_form($form,&$form_state,$id){
	$values_null = array('Addr2','Contact','Remark','ReceiveFlag','BillFlag');
	//dsm($id);
	$form['job'] = array('#type' => 'hidden', '#value' => $id);
	$form['values_null'] = array('#type' => 'hidden', '#value' => $values_null);
	$form['#tree'] = TRUE;
	$form['information'] = array(
			'#type' => 'fieldset',
			'#title' => t('ข้อมูลสถายที่รับสินค้าและวัตถุดิบ'),
			'#collapsible' => TRUE,
			'#collapsed' => FALSE,
	);
	$form['information']['DropShipID'] = array(
			'#type' => 'textfield',
			'#title' => t('ไอดี สถานที่'),
			'#disabled' => TRUE,
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	$form['information']['DropShipCode'] = array(
			'#type' => 'textfield',
			'#title' => t('รหัส สถานที่'),
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	$form['information']['DropShipName'] = array(
			'#type' => 'textfield',
			'#title' => t('ชื่อ สถานที่'),
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	$form['information']['Addr21'] = array(
			'#type' => 'textfield',
			'#title' => t('ที่อยู่ '),
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	$form['information']['District'] = array(
			'#type' => 'textfield',
			'#title' => t('ตำบล'),
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	$form['information']['Amphur'] = array(
			'#type' => 'textfield',
			'#title' => t('อำเภอ'),
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	$form['information']['Province'] = array(
			'#type' => 'textfield',
			'#title' => t('จังหวัด'),
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	$form['information']['PostCode'] = array(
			'#type' => 'textfield',
			'#title' => t('รหัสไปรษณีย์'),
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	$form['information']['Tel'] = array(
			'#type' => 'textfield',
			'#title' => t('เบอร์โทรศัพท์'),
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	$form['information']['Fax'] = array(
			'#type' => 'textfield',
			'#title' => t('เบอร์แฟกซ์'),
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	$form['submit'] = array(
			'#type' => 'submit',
			'#value' => t('บันทึก'));
	///
	if ($id!='add'){
		$emreceive_place_values = kc_emreceive_place_get_data($id);
//		dsm($emreceive_place_values);
		foreach ($emreceive_place_values as $key=>$values){
			if (!(in_array($key, $values_null))){
//				dsm($key.$values);
				$form['information'][$key]['#default_value'] = $values;
			}
		}
	}
//	$form_state['redirect'] = ;
	return $form;
}
function kc_emreceive_place_form_submit($form,&$form_state){
//	
	$form_state['rebuild'] = TRUE;
	$form_state['redirect'] = "/kc_emreceive_place/view";
	$values = $form_state['values'];
	$emreceive_place_values = $values['information'];
	dsm($form_state);
	dsm($values);
//	dsm($emreceive_place_values);
	if ($values['job']=='add'){
		$ret = db_query('select max(DropShipID)+1 from EMReceivePlace')->fetchCol();
		$id = $ret[0];
		$emreceive_place_values['DropShipID'] = $id;
		$emreceive_place = entity_create('EMReceivePlace', $emreceive_place_values);
		$emreceive_place->save();
	}else{
		$emreceive_place_in_base = kc_emreceive_place_get_data($emreceive_place_values['DropShipID']);
		foreach ($values['values_null'] as $key){
			unset($emreceive_place_in_base[$key]);
		}
		$arr = array_diff($emreceive_place_in_base, $emreceive_place_values);
		//$arr = array_diff_uassoc($array1, $array2, $key_compare_func)($emreceive_place_in_base, $emreceive_place_values);
		dsm($arr);
		dsm(count($arr));
		if (count($arr)>0)
			$update = kc_emreceive_place_update_data($emreceive_place_values);
	}
	drupal_goto("/kc_emreceive_place/view");
	
}

?>