<?php
function kc_emreceive_place_views_default_views() {

	// exported view goes here

	$view = new view();
	$view->name = 'emreceiveplace';
	$view->description = '';
	$view->tag = 'default';
	$view->base_table = 'EMReceivePlace';
	$view->human_name = 'EMReceivePlace';
	$view->core = 7;
	$view->api_version = '3.0';
	$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

	/* Display: Master */
	$handler = $view->new_display('default', 'Master', 'default');
	$handler->display->display_options['title'] = 'EMReceivePlace';
	$handler->display->display_options['use_more_always'] = FALSE;
	$handler->display->display_options['access']['type'] = 'none';
	$handler->display->display_options['cache']['type'] = 'none';
	$handler->display->display_options['query']['type'] = 'views_query';
	$handler->display->display_options['exposed_form']['type'] = 'basic';
	$handler->display->display_options['pager']['type'] = 'full';
	$handler->display->display_options['pager']['options']['items_per_page'] = '20';
	$handler->display->display_options['pager']['options']['offset'] = '0';
	$handler->display->display_options['pager']['options']['id'] = '0';
	$handler->display->display_options['pager']['options']['quantity'] = '9';
	$handler->display->display_options['style_plugin'] = 'table';
	/* Header: Global: PHP */
	$handler->display->display_options['header']['php']['id'] = 'php';
	$handler->display->display_options['header']['php']['table'] = 'views';
	$handler->display->display_options['header']['php']['field'] = 'php';
	$handler->display->display_options['header']['php']['php_output'] = '<?php
			echo "<a href=\'add\'>เพิ่ม </a>";
			?>
			';
	/* Field: EMReceivePlace: DropShipID */
	$handler->display->display_options['fields']['DropShipID']['id'] = 'DropShipID';
	$handler->display->display_options['fields']['DropShipID']['table'] = 'EMReceivePlace';
	$handler->display->display_options['fields']['DropShipID']['field'] = 'DropShipID';
	$handler->display->display_options['fields']['DropShipID']['separator'] = '';
	/* Field: EMReceivePlace: DropShipCode */
	$handler->display->display_options['fields']['DropShipCode']['id'] = 'DropShipCode';
	$handler->display->display_options['fields']['DropShipCode']['table'] = 'EMReceivePlace';
	$handler->display->display_options['fields']['DropShipCode']['field'] = 'DropShipCode';
	/* Field: EMReceivePlace: DropShipName */
	$handler->display->display_options['fields']['DropShipName']['id'] = 'DropShipName';
	$handler->display->display_options['fields']['DropShipName']['table'] = 'EMReceivePlace';
	$handler->display->display_options['fields']['DropShipName']['field'] = 'DropShipName';
	$handler->display->display_options['fields']['DropShipName']['label'] = 'ชื่อ';
	/* Field: EMReceivePlace: Addr21 */
	$handler->display->display_options['fields']['Addr21']['id'] = 'Addr21';
	$handler->display->display_options['fields']['Addr21']['table'] = 'EMReceivePlace';
	$handler->display->display_options['fields']['Addr21']['field'] = 'Addr21';
	/* Field: EMReceivePlace: District */
	$handler->display->display_options['fields']['District']['id'] = 'District';
	$handler->display->display_options['fields']['District']['table'] = 'EMReceivePlace';
	$handler->display->display_options['fields']['District']['field'] = 'District';
	/* Field: EMReceivePlace: Amphur */
	$handler->display->display_options['fields']['Amphur']['id'] = 'Amphur';
	$handler->display->display_options['fields']['Amphur']['table'] = 'EMReceivePlace';
	$handler->display->display_options['fields']['Amphur']['field'] = 'Amphur';
	/* Field: EMReceivePlace: Province */
	$handler->display->display_options['fields']['Province']['id'] = 'Province';
	$handler->display->display_options['fields']['Province']['table'] = 'EMReceivePlace';
	$handler->display->display_options['fields']['Province']['field'] = 'Province';
	/* Field: EMReceivePlace: PostCode */
	$handler->display->display_options['fields']['PostCode']['id'] = 'PostCode';
	$handler->display->display_options['fields']['PostCode']['table'] = 'EMReceivePlace';
	$handler->display->display_options['fields']['PostCode']['field'] = 'PostCode';
	/* Field: EMReceivePlace: Tel */
	$handler->display->display_options['fields']['Tel']['id'] = 'Tel';
	$handler->display->display_options['fields']['Tel']['table'] = 'EMReceivePlace';
	$handler->display->display_options['fields']['Tel']['field'] = 'Tel';
	/* Field: EMReceivePlace: Fax */
	$handler->display->display_options['fields']['Fax']['id'] = 'Fax';
	$handler->display->display_options['fields']['Fax']['table'] = 'EMReceivePlace';
	$handler->display->display_options['fields']['Fax']['field'] = 'Fax';
	/* Field: Global: Custom text */
	$handler->display->display_options['fields']['nothing']['id'] = 'nothing';
	$handler->display->display_options['fields']['nothing']['table'] = 'views';
	$handler->display->display_options['fields']['nothing']['field'] = 'nothing';
	$handler->display->display_options['fields']['nothing']['alter']['text'] = 'แก้ไข';
	$handler->display->display_options['fields']['nothing']['alter']['make_link'] = TRUE;
	$handler->display->display_options['fields']['nothing']['alter']['path'] = 'kc_emreceive_place/[DropShipID]/edit';

	/* Display: Page */
	$handler = $view->new_display('page', 'Page', 'page');
	$handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
	$handler->display->display_options['path'] = 'kc_emreceive_place/view';


	$views[$view->name] = $view;
	// return views
	return $views;
}
?>