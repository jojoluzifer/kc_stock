<?php
function kc_emreceive_place_form($form,&$form_state,$id){
	$values_null = array('Addr2','Contact','Remark','ReceiveFlag','BillFlag');
	//dsm($id);
	$form['job'] = array('#type' => 'hidden', '#value' => $id);
	$form['values_null'] = array('#type' => 'hidden', '#value' => $values_null);
	$form['#tree'] = TRUE;
	$form['information'] = array(
			'#type' => 'fieldset',
			'#title' => t('ข้อมูลสถายที่รับสินค้าและวัตถุดิบ'),
			'#collapsible' => TRUE,
			'#collapsed' => FALSE,
	);
	$form['information']['DropShipID'] = array(
			'#type' => 'textfield',
			'#title' => t('ไอดี สถานที่'),
			'#disabled' => TRUE,
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	$form['information']['DropShipCode'] = array(
			'#type' => 'textfield',
			'#title' => t('รหัส สถานที่'),
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	$form['information']['DropShipName'] = array(
			'#type' => 'textfield',
			'#title' => t('ชื่อ สถานที่'),
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	$form['information']['Addr21'] = array(
			'#type' => 'textfield',
			'#title' => t('ที่อยู่ '),
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	$form['information']['District'] = array(
			'#type' => 'textfield',
			'#title' => t('ตำบล'),
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	$form['information']['Amphur'] = array(
			'#type' => 'textfield',
			'#title' => t('อำเภอ'),
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	$form['information']['Province'] = array(
			'#type' => 'textfield',
			'#title' => t('จังหวัด'),
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	$form['information']['PostCode'] = array(
			'#type' => 'textfield',
			'#title' => t('รหัสไปรษณีย์'),
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	$form['information']['Tel'] = array(
			'#type' => 'textfield',
			'#title' => t('เบอร์โทรศัพท์'),
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	$form['information']['Fax'] = array(
			'#type' => 'textfield',
			'#title' => t('เบอร์แฟกซ์'),
			//'#default_value' => $node->title,
			//'#required' => TRUE,
	);
	$form['submit'] = array(
			'#type' => 'submit',
			'#value' => t('บันทึก'));
	///
	if ($id!='add'){
		$emreceive_place_values = kc_emreceive_place_get_data($id);
//		dsm($emreceive_place_values);
		foreach ($emreceive_place_values as $key=>$values){
			if (!(in_array($key, $values_null))){
//				dsm($key.$values);
				$form['information'][$key]['#default_value'] = $values;
			}
		}
	}
//	$form_state['redirect'] = ;
	return $form;
}
function kc_emreceive_place_form_submit($form,&$form_state){
//	
	$form_state['rebuild'] = TRUE;

	drupal_goto("/kc_emreceive_place/view");
	
}

?>